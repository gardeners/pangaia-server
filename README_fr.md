
<a href="https://laravel.com">
  <img src="https://img.shields.io/badge/%20%20-Laravel%20-grey?logo=laravel" alt="Badge Laravel" width="145"/>
</a>
<a href="https://www.docker.com/">
  <img src="https://img.shields.io/badge/%20%20-Docker%20-grey?logo=docker" alt="Badge Docker" width="140"/>
</a>
<a href="https://vuejs.org/">
  <img src="https://img.shields.io/badge/%20%20-VueJS%20-grey?logo=vue.js" alt="Badge VueJS" width="127"/>
</a>

# Présentation du projet

***Pangaia*** est une application web et mobile, qui aide et assiste les jardiniers dans la planification de leurs tâches.

# Dépendances
Ce projet nécessite:
 - **PHP** *8.1* avec les extensions:
   - *PHP-Mbstring*
   - *PHP-XML*
   - *PHP-Curl*
   - *PHP-Zip*
   - *PHP-FileInfo*
 - **Composer** *2.5*
 - **Docker** *20.10*
 - **NPM** *9.6*
 - **Docker-compose** *1.29*
 - **NodeJS** *20.2*

# Installation
## SSH 
```shell
git clone git@framagit.org:gardeners/pangaia-server.git 
```
```shell
cd pangaia-server
```
```shell
make install
```
## HTTP
```shell
git clone https://framagit.org/gardeners/pangaia-server.git 
```
```shell
cd pangaia-server
```
```shell
make install
```

# Démarrer le projet
## En mode dévélopement 
```shell
make dev
```
## En mode production
```shell
make build
```
# Structure du projet
## Conteneurs docker  
- **laravel.test** : Notre projet
- **mysql** : Base de données
- **mailpit** : Serveur SMTP pour les mails
- **caddy** : Serveur HTTPS 

## Laravel
- **laravel/fortify** : Fournit des contrôleurs backend et des  squelettes pour l'authentification Laravel.
- **laravel/framework** : Le cadre de travail principal de Laravel.
- **laravel/jetstream** : Fournit une structure de base pour Laravel utilisant le framework CSS Tailwind.
- **laravel/sail** : Fournit des fichiers Docker pour exécuter une application Laravel de base.
- **laravel/sanctum** : Offre un système d'authentification léger pour les applications à page unique (SPA) et les API simples.
- **laravel/tinker** : Un puissant REPL (Read-Eval-Print Loop) pour le framework Laravel.

## API
- **DarkaOnLine/L5-Swagger** : Swagger génère automatiquement des fichiers de documentation Swagger/OpenAPI à partir de nos commentaires de code. Swagger offre une interface utilisateur interactive. 

## Routes
 - Api GET + token: http://pangaia.local/api/user
 - Back office : http://pangaia.local/admin/dashboard
 - Documentation api : http://pangaia.local/api/documentation
 - Admin Documentation api : http://pangaia.local/api/admin/documentation

Pour voir toutes les routes du projet:
```shell
php artisan route:list
```

# License

[![Hippocratic License HL3-CORE](https://img.shields.io/static/v1?label=Hippocratic%20License&message=HL3-CORE&labelColor=5e2751&color=bc8c3d)](https://firstdonoharm.dev/version/3/0/core.html)
