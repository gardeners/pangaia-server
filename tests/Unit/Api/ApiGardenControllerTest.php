<?php

namespace Tests\Unit\Api;

use App\Models\Garden;
use Database\Factories\TeamFactory;
use Symfony\Component\HttpFoundation\Response;

class ApiGardenControllerTest extends ApiTestCase
{
    protected string $route;
    protected $user;
    protected $model;
    protected array $mock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->route = "/api/v1/garden";
        $this->model = Garden::class;
        $this->mock = [
            'name' => "Default garden name",
            'personal_team' => true,
        ];

        $this->attrChecklist_for_ApiResource = [
            "name",
            "personal_team"
        ];
    }

    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get($this->route);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo Users
     */
    public function test_demo_user_should_be_able_to_crud_gardens(): void
    {
        # Check that all CRUD opetations are possible.
        $this->actingAs($this->demoUser);
        $id = $this->createMockSuccessfully();
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id, $this->demoUser);
        $this->deleteMockSuccessfully($id);
    }

    public function test_user_cannot_crud_others_garden(): void
    {
        # Check that all CRUD opetations are possible.
        $this->actingAs($this->demoUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->defaultUser);
        $this->readMockUnsuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    public function test_user_can_access_shared_garden(): void
    {
        $this->actingAs($this->sharedWithDefaultUser);
        $id = $this->createMockSuccessfully();
        $this->defaultUser->teams()->save(Garden::where('name', $this->mock['name'])->first());
        $this->actingAs($this->defaultUser);
        $this->readMockSuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }
    public function test_admin_can_access_others_gardens(): void
    {
        $this->actingAs($this->defaultUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->adminUser);
        $this->readMockSuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }
}
