<?php

namespace Tests\Unit\Api;

use App\Models\Plant;
use App\Models\Garden;
use App\Models\Repositories\ZoneRepository;
use App\Models\Zone;
use Symfony\Component\HttpFoundation\Response;

class ApiPlantControllerTest extends ApiTestCase
{
    protected string $route;
    protected $model;
    protected array $mock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->route = '/api/v1/plant';
        $this->model = Plant::class;
        $this->mock = $this->model::factory()->definition();

        $this->attrChecklist_for_ApiResource = [
            "name",
            "description"
        ];
    }

    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get();
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo users.
     */
    public function test_demo_user_can_see_his_plants(): void
    {
        // A demo user can query his zone.
        $this->actingAs($this->demoUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $plants = $response->getData()->data;
        $this->assertEquals(0, count($plants));
    }

    public function test_demo_user_can_crud_a_plant_properly(): void
    {
        // A demo user can create, update nor delete a plant

        $this->actingAs($this->demoUser);
        $this->mock["zone_id"] = (new ZoneRepository(new Zone()))->getByUser($this->demoUser)[0]->id;
        $id = $this->createMockSuccessfully();
        $this->readMockSuccessfully($id, $this->demoUser);
        $this->updateMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }


    /**
     * Classic Test Users.
     *
     * A classic user can interact with his plants. 
     * Plants are in a zones, zones are in gardens and gardens can be shared amongst users.
     *
     * By default, all methods interact with $this->route
     * Creation or update tries to send the $this->mock to the routes.
     * Read or delete uses the $this->route and the id.
     */

    public function test_user_can_see_his_plants(): void
    {
        $this->actingAs($this->defaultUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $plants = $response->getData()->data;
        $this->assertGreaterThan(0, count($plants));
        $this->assertLessThan(10, count($plants));


    }
    public function test_user_can_crud_a_plant(): void
    {
        $this->actingAs($this->defaultUser);
        $this->mock["zone_id"] = (new ZoneRepository(new Zone()))->getByUser($this->defaultUser)[0]->id;
        $id = $this->createMockSuccessfully();

        $this->mock['description'] = fake()->sentence();
        $this->updateMockSuccessfully($id);

        $this->readMockSuccessfully($id, $this->defaultUser);
        $this->deleteMockSuccessfully($id);
    }

    public function test_user_cannot_modify_unrelated_plant(): void
    {
        $this->actingAs($this->adminUser);

        $this->mock["zone_id"] = (new ZoneRepository(new Zone()))->getByUser($this->adminUser)[0]->id;
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->defaultUser);
        $this->updateMockUnsuccessfully($id);
    }

    public function test_user_cannot_delete_unrelated_plant(): void
    {
        $this->actingAs($this->adminUser);
        $this->mock["zone_id"] = (new ZoneRepository(new Zone()))->getByUser($this->adminUser)[0]->id;
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->defaultUser);
        $this->deleteMockUnsuccessfully($id);
    }

    public function test_user_can_add_in_shared_garden(): void
    {
        // Some gardens can be shared amonst users.
        // Even if the user is not the primary owner, he can post.

        $this->actingAs($this->defaultUser); # Act as Paul
        $this->mock["zone_id"] = Garden::where('name', "Bapt & Paul's Garden")->first()->zones[0]->id; # Shared garden owned by Paul
        $this->actingAs($this->sharedWithDefaultUser);
        $id = $this->createMockSuccessfully();
        $this->deleteMockSuccessfully($id);
    }


    /** 
     * Admin Users.
     * 
     * A admin user can interact with his plants and plants of others. 
     * 
     **/
    public function test_admin_can_crud_plants(): void
    {
        $this->actingAs($this->adminUser);

        $id = $this->createMockSuccessfully();
        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id, $this->adminUser);
        $this->deleteMockSuccessfully($id);
    }
    public function test_admin_can_delete_unrelated_plant(): void
    {
        $this->actingAs($this->defaultUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->adminUser);
        $this->deleteMockSuccessfully($id);
    }

    public function test_admin_can_add_in_other_gardens(): void
    {
        // Some gardens can be shared amonst users. 
        // Even if the user is not the primary owner, he can post.

        $this->actingAs($this->adminUser);
        $this->mock["zone"] = Garden::where('name', "Bapt & Paul's Garden")->first()->zones[0]->id; # Shared garden owned by Paul
        $id = $this->createMockSuccessfully();
        $this->deleteMockSuccessfully($id);
    }
}
