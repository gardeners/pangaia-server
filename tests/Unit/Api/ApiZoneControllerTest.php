<?php

namespace Tests\Unit\Api;

use App\Models\Garden;
use App\Models\Zone;

use Symfony\Component\HttpFoundation\Response;

class ApiZoneControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->route = '/api/v1/zone';
        $this->model = Zone::class;
        $this->mock = $this->model::factory()->definition();

        $this->attrChecklist_for_ApiResource = [
            "name",
            "width",
            "height",
            "position_top",
            "position_left"
        ];
    }


    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get();
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo users.
     */
    public function test_demo_user_can_see_his_zone(): void
    {
        // A demo user can query his zone.

        $this->actingAs($this->demoUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $zones = $response->getData()->data;
        $this->assertGreaterThan(0, count($zones));
    }

    public function test_demo_user_cannot_crud_a_zone_properly(): void
    {
        // A demo user cannot create, update nor delete a zone
        // He can only see his zone.

        $this->actingAs($this->demoUser);
        $this->createMockUnsuccessfully();
        $this->actingAs($this->defaultUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->demoUser);
        $this->mock['name'] = fake()->name();
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }


    /**
     * Classic Test Users.
     *
     * A classic user can interact with his zones.
     * Zones are in a garden and gardens can be shared among users.
     *
     * By default, all methods interact with $this->route
     * Creation or update tries to send the $this->mock to the routes.
     * Read or delete uses the $this->route and the id.
     */

    public function test_user_can_crud_a_zone(): void
    {
        $this->actingAs($this->defaultUser);

        $this->mock['garden_id'] = $this->defaultUser->currentTeam()->first()->id;
        $id = $this->createMockSuccessfully();
        $this->mock['width'] = 100;
        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id, $this->defaultUser);
        $this->deleteMockSuccessfully($id);
    }
    public function test_user_cannot_delete_unrelated_zone(): void
    {
        $this->actingAs($this->adminUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->defaultUser);
        $this->mock['name'] = fake()->name();
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    public function test_user_can_crud_in_shared_garden(): void
    {
        $this->actingAs($this->sharedWithDefaultUser);
        $this->mock['garden_id'] = Garden::where('name', "Bapt & Paul's Garden")->first()->id;
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->defaultUser);
        $this->readMockSuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }
}
