<?php

namespace Tests\Unit\Api;

use App\Models\PlantDefinition;
use Symfony\Component\HttpFoundation\Response;

class ApiPlantDefinitionControllerTest extends ApiTestCase
{
    protected string $route;
    protected  $model;
    protected array $mock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->route = '/api/v1/plant-definition';
        $this->model = PlantDefinition::class;

        $this->mock = $this->model::factory()->definition();
        $this->mock['name'] = [fake()->name()];

        $this->attrChecklist_for_ApiResource = [
            "description"
        ];
    }

    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get();
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo users.
     */
    public function test_demo_user_can_see_plant_definitions(): void
    {
        // A demo user can query plant definitions

        $this->actingAs($this->demoUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $plants = $response->getData()->data;
        $this->assertGreaterThan(0, count($plants));
    }

    public function test_demo_user_can_see_most_common_name(): void
    {
        $this->actingAs($this->demoUser);
        $this->assertEquals('Le plantain lancéolé', PlantDefinition::find(1)->name);
    }

    public function test_demo_user_cannot_crud_a_plant_definition(): void
    {
        // A demo user cannot create, update nor delete a plant definition
        $this->actingAs($this->demoUser);
        $this->createMockUnsuccessfully();
        $this->actingAs($this->defaultUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->demoUser);
        $this->mock['name'] = [fake()->name()];
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }


    /**
     * Classic Test Users.
     *
     * A classic user can interact with his plants.
     * Plants are related to plant definitions and a classic user can update those definitions.
     */

    public function test_user_can_see_plant_definitions(): void
    {
        $this->actingAs($this->defaultUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $plants = $response->getData()->data;
        $this->assertGreaterThan(0, count($plants));
    }

    public function test_user_can_crud_plant_definitions(): void
    {
        $this->actingAs($this->defaultUser);
        $id = $this->createMockSuccessfully();
        $this->assertEquals($this->mock['name'][0], PlantDefinition::find($id)->name);
        $this->mock['name'] = [fake()->name()];
        $this->mock['description'] = fake()->sentence();
        $this->updateMockSuccessfully($id);
        $this->assertEquals($this->mock['name'][0], PlantDefinition::find($id)->name);
        $this->readMockSuccessfully($id, $this->defaultUser);
        $this->deleteMockSuccessfully($id);
    }

    /**
     * Admin Users.
     *
     * A admin user can interact with plant definitions.
     *
     **/
    public function test_admin_can_crud_plant_definitions(): void
    {
        $this->actingAs($this->adminUser);

        $id = $this->createMockSuccessfully();
        $this->assertEquals($this->mock['name'][0], PlantDefinition::find($id)->name);

        $this->mock['name'] = [fake()->name(), fake()->name()];
        $this->mock['description'] = fake()->sentence();
        $this->updateMockSuccessfully($id);
        $this->assertEquals($this->mock['name'][0], PlantDefinition::find($id)->name);

        $this->readMockSuccessfully($id, $this->adminUser);
        $this->deleteMockSuccessfully($id);
    }
}
