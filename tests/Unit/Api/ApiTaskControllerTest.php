<?php

namespace Tests\Unit\Api;

use App\Models\Plant;
use App\Models\Repositories\PlantRepository;
use App\Models\Repositories\ZoneRepository;
use App\Models\Task;
use App\Models\TaskDefinition;
use App\Models\Zone;
use Symfony\Component\HttpFoundation\Response;

class ApiTaskControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->route = '/api/v1/task';
        $this->model = Task::class;

        $this->mock = $this->model::factory()->definition();

        $this->attrChecklist_for_ApiResource = [
            "name",
            "description",
            "date_beg",
            "date_end"
        ];
    }


    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get();
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo users.
     */
    public function test_demo_user_can_see_tasks(): void
    {
        // A demo user can query his tasks.
        $this->actingAs($this->demoUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $tasks = $response->getData()->data;
        $this->assertGreaterThan(0, count($tasks));
    }

    public function test_demo_user_can_crud_a_task(): void
    {
        // A demo user can create, update or delete a task
        $this->actingAs($this->demoUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->demoUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }

    public function test_demo_user_cannot_crud_someone_elses_task(): void
    {
        // A demo user cannot read, update or delete a task owned by someone else
        $this->actingAs($this->defaultUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->defaultUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->demoUser);
        $this->readMockUnsuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    /** 
     * Classic Test Users.
     * 
     * A classic user can read tasks  and implement new ones.
     * He cannot access tasks owned by someone else.
     */

    public function test_user_can_see_tasks(): void
    {
        $this->actingAs($this->defaultUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $task_defs = $response->getData()->data;
        $this->assertGreaterThan(0, count($task_defs));
    }
    public function test_user_can_crud_tasks(): void
    {
        $this->actingAs($this->defaultUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->defaultUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }


    public function test_user_has_to_set_an_end_date_after_the_before_date(): void
    {
        $this->actingAs($this->defaultUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->defaultUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->mock['date_beg'] = fake()->dateTimeBetween($this->mock['date_end'], '+1 month')->format('Y-m-d');
        $this->updateMockUnsuccessfully($id, Response::HTTP_OK, ['Validation errors', 'The date end field must be a date after or equal to date beg.']);
        $this->deleteMockSuccessfully($id);
    }
    public function test_user_cannot_crud_someone_elses_task(): void
    {
        // A classic user cannot read, update or delete a task owned by someone else
        $this->actingAs($this->adminUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->adminUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->defaultUser);
        $this->readMockUnsuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    /**
     * Admin Users.
     *
     * A admin user can interact with tasks. Even those owned by someone else. 
     *
     **/
    public function test_admin_can_crud_his_tasks(): void
    {
        $this->actingAs($this->adminUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->adminUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }
    public function test_admin_can_crud_someone_elses_task(): void
    {
        // A admin user can read, update or delete a task owned by someone else
        $this->actingAs($this->defaultUser);
        $this->mock['plant_id'] = Plant::factory()->create([
            'zone_id' => (new ZoneRepository(new Zone()))->getByUser($this->defaultUser)[0]->id
        ])->id;
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->adminUser);
        $this->readMockSuccessfully($id);
        $this->mock['name'] = fake()->name();
        $this->updateMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }
}
