<?php

namespace Tests\Unit\Api;

use Illuminate\Testing\Exceptions\InvalidArgumentException;
use Illuminate\Testing\TestResponse;

trait ApiMockTrait
{
    protected array $mock;
    protected string $route;

    protected function createAMock($mock = null)
    {
        return $this->post($this->route, $mock ?? $this->mock);
    }
    protected function updateAMock($id)
    {
        return $this->update($id);
    }

    protected function readAMock($id): TestResponse
    {
        return $this->get($id);
    }
    protected function deleteAMock($id)
    {
        return $this->delete($id);
    }


    /**
     * CRUD methods.
     * Always try to apply the mock on the route
     */
    public function get($uri_or_id = null, array $headers = [])
    {
        // Calls $this->route by default.
        // if uri of type string specified, direcly calls the uri
        // if id specified, adds the id to the route

        $uri = $uri_or_id;
        if (empty($uri)) {
            $uri = $this->route;
        }
        if (is_numeric($uri)) {
            $uri = "$this->route/$uri";
        }
        return parent::get($uri, $headers);
    }
    public function post($uri = null, array $data = [], array $headers = [])
    {
        // Calls $this->route by default.
        if (is_null($uri)) {
            $uri = $this->route;
        }
        if (empty($data)) {
            $data = $this->mock;
        }
        return parent::post($uri, $data, $headers);
    }

    public function update($id, array $data = null, array $headers = [])
    {
        // Calls $this->route.

        $uri = "$this->route/$id";

        if (is_null($data)) {
            $data = $this->mock;
        }

        return parent::put($uri, $data, $headers);
    }

    public function delete($uri_or_id = null, array $data = [], array $headers = [])
    {
        // Calls $this->route by default.

        if (is_null($uri_or_id)) {
            throw new InvalidArgumentException('Invalid use of method, please specify an id to delete for known route');
        }
        $uri = $uri_or_id;
        if (is_numeric($uri_or_id)) {
            $uri = "$this->route/$uri_or_id";
        }
        return parent::delete($uri, $data, $headers);
    }
}
