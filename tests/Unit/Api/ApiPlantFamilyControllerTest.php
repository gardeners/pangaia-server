<?php

namespace Tests\Unit\Api;

use App\Models\PlantFamily;

use SebastianBergmann\Type\VoidType;
use Symfony\Component\HttpFoundation\Response;

class ApiPlantFamilyControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->route = '/api/v1/plant-family';
        $this->model = PlantFamily::class;

        $this->mock = $this->model::factory()->definition();

        $this->attrChecklist_for_ApiResource = [
            "name",
        ];
    }


    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get();
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo users.
     */
    public function test_demo_user_can_see_plant_families(): void
    {
        // A demo user can query plant families

        $this->actingAs($this->demoUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $plants = $response->getData()->data;
        $this->assertGreaterThan(0, count($plants));
    }

    public function test_demo_user_cannot_crud_plant_families(): void
    {
        $this->actingAs($this->demoUser);
        $this->createMockUnsuccessfully();
        $this->actingAs($this->adminUser);
        $id = $this->createMockSuccessfully();
        $this->actingAs($this->demoUser);
        $this->mock['name'] = fake()->name();
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }


    /** 
     * Classic Test Users.
     * 
     * A classic user can only read plant families. 
     */

    public function test_user_can_see_plant_families(): void
    {
        $this->actingAs($this->defaultUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $plants = $response->getData()->data;
        $this->assertGreaterThan(0, count($plants));
    }
    public function test_user_cannot_crud_plant_families(): void
    {
        $this->actingAs($this->defaultUser);
        $this->createMockUnsuccessfully();
        $this->actingAs($this->adminUser);
        $id = $this->createMockSuccessfully();
        $this->mock['name'] = fake()->name();
        $this->actingAs($this->defaultUser);
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    /** 
     * Admin Users.
     * 
     * A admin user can interact with plant families. 
     * 
     **/
    public function test_admin_can_crud_plant_families(): void
    {
        $this->actingAs($this->adminUser);

        $id = $this->createMockSuccessfully();

        $this->mock['name'] = fake()->name();
        $this->mock['description'] = fake()->sentence();

        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id, $this->adminUser);
        $this->deleteMockSuccessfully($id);
    }
}
