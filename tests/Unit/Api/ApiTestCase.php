<?php

namespace Tests\Unit\Api;

use App\Models\User;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Testing\TestResponse;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class ApiTestCase extends TestCase
{
    use RefreshDatabase;

    // Seed the database automatically for the tests
    protected $seed = true;

    // API Test case template
    // Based on the mock, generates all generic methods for Pangaia's api test cases.

    use ApiMockTrait;

    // user account having a Demo role
    protected User $demoUser;

    // user account having a classic role
    protected User $defaultUser;

    protected User $sharedWithDefaultUser;

    // user account having a classic role
    protected User $adminUser;

    protected $model; // TODO : Find it's type...

    // checklist of attributes to check in the api response to compare with the current mock
    protected array $attrChecklist_for_ApiResource;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withHeaders([
            'Accept' => 'application/json'
        ]);

        $this->demoUser = User::where('name', 'demo')->first();
        $this->defaultUser = User::where('name', 'paul')->first();
        $this->sharedWithDefaultUser = User::where('name', 'baptiste')->first();
        $this->adminUser = User::where('name', 'docus')->first();
    }


    protected function createMockSuccessfully(): int
    {
        $response = $this->createAMock();
        $response->assertSuccessful();
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertTrue(property_exists($response->getData(), 'data'));

        // Response should contain the id of the generated object
        $this->assertTrue(property_exists($response->getData()->data, 'id'));
        $id = $response->getData()->data->id;
        $this->assertIsNumeric($id);

        // Check model exists in db
        $object = $this->model::find($id);
        $this->assertNotNull($object);

        return $id;
    }
    protected function createMockUnsuccessfully($mock = null, int $httpStatus = Response::HTTP_FORBIDDEN, array $expected_content = []): TestResponse
    {
        $response = $this->createAMock($mock ?? $this->mock);
        $response->assertStatus($httpStatus);
        foreach ($expected_content as $content) {
            $this->assertStringContainsString($content, $response->getContent());
        }
        return $response;
    }
    protected function readMockSuccessfully($id): TestResponse
    {
        $response = $this->readAMock($id);
        $response->assertSuccessful();
        $this->assertApiResource($response);
        return $response;
    }
    protected function readMockUnsuccessfully($id): TestResponse
    {
        $response = $this->readAMock($id);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        return $response;
    }
    protected function updateMockSuccessfully($id): TestResponse
    {

        $response = $this->updateAMock($id);
        $response->assertSuccessful();
        $response->assertStatus(Response::HTTP_ACCEPTED);


        // Check attributes
        $this->assertApiResource($response);

        return $response;
    }
    protected function updateMockUnsuccessfully($id, int $httpStatus = Response::HTTP_FORBIDDEN, array $expected_content = []): TestResponse
    {
        $response = $this->updateAMock($id);
        $response->assertStatus($httpStatus);
        foreach ($expected_content as $content) {
            $this->assertStringContainsString($content, $response->getContent());
        }
        return $response;
    }
    protected function deleteMockUnsuccessfully($id): TestResponse
    {
        $response = $this->deleteAMock($id);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
        return $response;
    }
    protected function deleteMockSuccessfully($id): TestResponse
    {
        $response = $this->deleteAMock($id);
        $response->assertStatus(Response::HTTP_NO_CONTENT);

        // Check model no longer exists in db
        $plant = $this->model::find($id);
        $this->assertNull($plant);

        return $response;
    }
    private function assertApiResource($response)
    {
        // Assert that the JSON response contains proper values compared to $this->mock
        // $this->attrChecklist_for_ApiResource should contain a list of attributes to compare.
        $json = $response->getData()->data;
        foreach ($this->attrChecklist_for_ApiResource as $attribute) {
            $this->assertEquals($this->mock[$attribute], $json->$attribute);
        }
    }
}
