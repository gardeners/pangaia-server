<?php

namespace Tests\Unit\Api;

use App\Models\Saying;
use Database\Factories\SayingFactory;

use Symfony\Component\HttpFoundation\Response;

class ApiSayingControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->route = '/api/v1/saying';
        $this->model = Saying::class;
        $this->mock = $this->model::factory()->definition();

        $this->attrChecklist_for_ApiResource = [
            "title",
            "date_beg",
            "date_end"
        ];
    }


    /**
     * Non connected users.
     */
    public function test_unauthenticated_user_should_have_a_401(): void
    {
        // A non connected user cannot use the Api.
        $response = $this->get();
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Demo users.
     */
    public function test_demo_user_can_see_sayings(): void
    {
        // A demo user can query sayings

        $this->actingAs($this->demoUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $sayings = $response->getData()->data;
        $this->assertGreaterThan(0, count($sayings));
    }

    public function test_demo_user_cannot_crud_a_saying(): void
    {
        $this->actingAs($this->demoUser);
        $this->createMockUnsuccessfully();
        $this->actingAs($this->adminUser);
        $id = $this->createMockSuccessfully();
        $this->mock["title"] = fake()->sentence();
        $this->actingAs($this->demoUser);
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    /** 
     * Classic Test Users.
     * 
     * A classic user can read sayings.
     */

    public function test_user_can_see_sayings(): void
    {
        $this->actingAs($this->defaultUser);

        $response = $this->get();
        $response->assertStatus(Response::HTTP_OK);

        // Check select many
        $task_defs = $response->getData()->data;
        $this->assertGreaterThan(0, count($task_defs));
    }

    public function test_user_cannot_crud_sayings(): void
    {
        $this->actingAs($this->defaultUser);

        $this->createMockUnsuccessfully();
        $this->actingAs($this->adminUser);
        $id = $this->createMockSuccessfully();
        $this->mock["title"] = fake()->sentence();
        $this->actingAs($this->defaultUser);
        $this->updateMockUnsuccessfully($id);
        $this->deleteMockUnsuccessfully($id);
    }

    /** 
     * Admin Users.
     * 
     * A admin user can interact with sayings.
     * 
     **/
    public function test_admin_can_crud_sayings(): void
    {
        $this->actingAs($this->adminUser);
        $id = $this->createMockSuccessfully();
        $this->mock['title'] = fake()->sentence();
        $this->updateMockSuccessfully($id);
        $this->readMockSuccessfully($id);
        $this->deleteMockSuccessfully($id);
    }


    public function test_admin_has_to_set_an_end_date_after_the_before_date(): void
    {
        $this->actingAs($this->adminUser);

        // creation with wrong dates should fail
        $mock = $this->mock;
        [$mock['date_beg'], $mock['date_end']] = [$mock['date_end'], $mock['date_beg']]; // swap dates
        $this->createMockUnsuccessfully($mock, Response::HTTP_OK, ['Validation errors', 'The date end field must be a date after or equal to date beg.']);

        // update with wrong dates should also fail
        $id = $this->createMockSuccessfully();
        $this->mock['date_end'] = "2019-12-10";
        $response = $this->updateMockUnsuccessfully($id, Response::HTTP_OK, ['Validation errors', 'The date end field must be a date after or equal to date beg.']);

        // Cleaning up
        $this->deleteMockSuccessfully($id);
    }
}
