<?php

namespace Tests\Feature;

use App\Models\Hs\HsPlantDefinition;
use App\Models\PlantDefinition;
use Database\Seeders\DifficultySeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsPlantDefinitionFactoryTest extends TestCase
{
    use RefreshDatabase;
    public function setUp(): void
    {
        parent::setUp();
        $this->seed(DifficultySeeder::class);
    }

    /** @test */
    public function it_can_create_hs_plant_definition()
    {
        $hsPlantDefinition = HsPlantDefinition::factory()->create();

        $this->assertModelExists($hsPlantDefinition);
    }

    /** @test */
    public function it_belongs_to_a_plant_definition()
    {
        $plantDefinition = PlantDefinition::factory()->create();
        $hsPlantDefinition = HsPlantDefinition::factory()->create([
            'plant_definitions_id' => $plantDefinition->id,
        ]);
        $this->assertInstanceOf(PlantDefinition::class, $hsPlantDefinition->plant_definition);
        $this->assertEquals($plantDefinition->id, $hsPlantDefinition->plant_definition->id);
    }

    /** @test */
    public function it_has_many_hs_plant_definition_indications()
    {
        $hsPlantDefinition = HsPlantDefinition::factory()
            ->hasHsPlantDefinitionIndications(3)
            ->create();

        $this->assertCount(3, $hsPlantDefinition->hs_plant_definition_indication);
    }

    /** @test */
    public function it_can_update_hs_plant_definition()
    {
        $hsPlantDefinition = HsPlantDefinition::factory()->create();

        $newLevel = 5;
        $hsPlantDefinition->update(['level' => $newLevel]);

        $this->assertDatabaseHas('hs_plant_definitions', [
            'id' => $hsPlantDefinition->id,
            'level' => $newLevel,
        ]);
    }

    /** @test */
    public function it_can_delete_hs_plant_definition()
    {
        $hsPlantDefinition = HsPlantDefinition::factory()->create();

        $hsPlantDefinitionId = $hsPlantDefinition->id;
        $hsPlantDefinition->delete();

        $this->assertDatabaseMissing('hs_plant_definitions', [
            'id' => $hsPlantDefinitionId,
        ]);
    }
}
