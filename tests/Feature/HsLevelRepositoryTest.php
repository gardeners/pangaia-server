<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Hs\HsLevel;
use App\Models\Repositories\Hs\HsLevelRepository;

class HsLevelRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_retrieves_all_hs_levels()
    {
        $nb = 5;
        $repository = new HsLevelRepository(new HsLevel());
        $before = $repository->getAll();
        HsLevel::factory()->count($nb)->create();

        $after = $repository->getAll();

        $this->assertTrue(count($after) - count($before) == $nb);
    }

    /** @test */
    public function it_finds_hs_level_by_name()
    {
        $levelName = 'Advanced';
        HsLevel::factory()->create(['name' => $levelName]);

        $repository = new HsLevelRepository(new HsLevel());
        $levelFound = $repository->getByName($levelName);

        $this->assertEquals($levelName, $levelFound->name);
    }

    public function it_finds_hs_level_by_id()
    {
        $hsLevel = HsLevel::factory()->create();

        $repository = new HsLevelRepository(new HsLevel());
        $foundHsLevel = $repository->getById($hsLevel->id);

        $this->assertInstanceOf(HsLevel::class, $foundHsLevel);
        $this->assertEquals($hsLevel->id, $foundHsLevel->id);
    }
}
