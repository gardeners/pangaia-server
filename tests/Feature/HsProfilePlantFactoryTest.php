<?php

namespace Tests\Feature\Hs;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsProfile;
use App\Models\Hs\HsProfilePlant;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsProfilePlantFactoryTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
    parent::setUp();

    $this->seed(PermissionSeeder::class);

    }

    /** @test */
    public function it_can_create_a_hs_profile_plant()
    {
        $hsProfilePlant = HsProfilePlant::factory()->create();

        $this->assertDatabaseHas('hs_profile_plants', ['id' => $hsProfilePlant->id]);
    }
    /** @test */
    public function it_can_update_a_hs_profile_plant()
    {
        $hsProfilePlant = HsProfilePlant::factory()->create();

        $hsProfilePlant->update(['found_date' => '2020-01-01']);

        $this->assertEquals('2020-01-01', $hsProfilePlant->fresh()->found_date);
    }

    /** @test */
    public function it_can_delete_a_hs_profile_plant()
    {
        $hsProfilePlant = HsProfilePlant::factory()->create();

        $hsProfilePlantId = $hsProfilePlant->id;
        $hsProfilePlant->delete();

        $this->assertDatabaseMissing('hs_profile_plants', ['id' => $hsProfilePlantId]);
    }

    /** @test */
    public function it_belongs_to_hs_plant_definition()
    {
        $hsProfilePlant = HsProfilePlant::factory()->create();

        $this->assertInstanceOf(HsPlantDefinition::class, $hsProfilePlant->hs_plant_definition);
    }

    /** @test */
    public function it_belongs_to_hs_profile()
    {
        $hsProfilePlant = HsProfilePlant::factory()->create();

        $this->assertInstanceOf(HsProfile::class, $hsProfilePlant->hs_profile);
    }
}
