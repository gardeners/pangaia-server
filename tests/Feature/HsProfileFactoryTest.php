<?php

namespace Tests\Feature;

use App\Models\Hs\HsLevel;
use App\Models\Hs\HsProfile;
use App\Models\User;
use Database\Seeders\GardenerDocusSeeder;
use Database\Seeders\HsLevelSeeder;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsProfileFactoryTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;
    public function setUp(): void
    {
        parent::setUp();


    }

    /** @test */
    public function it_can_create_hs_profile()
    {
        $HsProfile = HsProfile::factory()->create();

        $this->assertModelExists($HsProfile);
    }

    /** @test */
    public function it_can_delete_hs_profile()
    {
        $HsProfile = HsProfile::factory()->create();

        $HsProfile->delete();

        $this->assertModelMissing($HsProfile);
    }

    /** @test */
    public function it_can_update_hs_profile()
    {
        $hsProfile = HsProfile::factory()->create();

        $newData = [
            'profile_picture' => 'newProfilePicture',
        ];
        $hsProfile->update($newData);

        $this->assertDatabaseHas('hs_profiles', $newData);
    }

    /** @test */
    public function it_can_count_hs_profiles()
    {
        $count = 5;
        $before = HsProfile::count();
        HsProfile::factory($count)->create();

        $this->assertEquals(HsProfile::count() - $before, $count);
    }

    /** @test */
    public function a_hs_profile_belongs_to_a_hs_level()
    {
        $hsLevel = HsLevel::factory()->create();

        $HsProfile = HsProfile::factory()->create(['level' => $hsLevel->id]);

        $this->assertInstanceOf(HsLevel::class, $HsProfile->hs_level);

        $this->assertEquals($hsLevel->id, $HsProfile->hs_level->id);
    }

    /** @test */
    public function a_hs_profile_belongs_to_a_user()
    {
        $user = User::factory()->create();

        $HsProfile = HsProfile::factory()->create(['user_id' => $user->id]);

        $this->assertInstanceOf(User::class, $HsProfile->user);

        $this->assertEquals($user->id, $HsProfile->user->id);
    }
}
