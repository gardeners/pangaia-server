<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Hs\HsLevel;

class HsLevelFactoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_create_hs_level()
    {
        $hsLevel = HsLevel::factory()->create();

        $this->assertDatabaseHas('hs_levels', [
            'id' => $hsLevel->id,
        ]);
    }

    /** @test */
    public function it_can_delete_hs_level()
    {
        $hsLevel = HsLevel::factory()->create();

        $id = $hsLevel->id;
        $hsLevel->delete();

        $this->assertDatabaseMissing('hs_levels', [
            'id' => $id,
        ]);
    }

    /** @test */
    public function it_can_update_hs_level()
    {
        $hsLevel = HsLevel::factory()->create();

        $hsLevel->update([
            'name' => 'Updated Name',
        ]);

        $this->assertDatabaseHas('hs_levels', [
            'id' => $hsLevel->id,
            'name' => 'Updated Name',
        ]);
    }

    /** @test */
    public function it_can_count_hs_levels()
    {
        $nb = 5;
        $before = HsLevel::all();
        HsLevel::factory()->count($nb)->create();
        $after = HsLevel::all();
        $this->assertTrue(count($after) - count($before) == $nb);
    }
}
