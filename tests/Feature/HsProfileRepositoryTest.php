<?php

namespace Tests\Unit;

use App\Models\Hs\HsProfile;
use App\Models\Repositories\Hs\HsProfileRepository;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsProfileRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

    }
    /** @test */
    public function it_can_retrieve_all_HsProfiles()
    {
        $nb_profiles = 3;
        $repository = new HsProfileRepository(new HsProfile());
        $allHsProfilesBefore = $repository->getAll();
        HsProfile::factory()->count($nb_profiles)->create();

        $repository = new HsProfileRepository(new HsProfile());
        $allHsProfiles = $repository->getAll();

        $this->assertEquals(count($allHsProfiles) - count($allHsProfilesBefore) == $nb_profiles, $allHsProfiles->count());
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $allHsProfiles);
    }

    /** @test */
    public function it_can_find_a_HsProfile_by_id()
    {
        $HsProfile = HsProfile::factory()->create();

        $repository = new HsProfileRepository(new HsProfile());
        $foundHsProfile = $repository->getById($HsProfile->id);

        $this->assertInstanceOf(HsProfile::class, $foundHsProfile);
        $this->assertEquals($HsProfile->id, $foundHsProfile->id);
    }
}
