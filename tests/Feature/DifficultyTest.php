<?php

namespace Tests\Feature;

use App\Models\Difficulty;
use App\Models\PlantDefinition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DifficultyTest extends TestCase
{
    use RefreshDatabase;

    protected $difficulty1;
    protected $difficulty2;
    protected $plantDefinition1;
    protected $plantDefinition2;

    protected function setUp(): void
    {
        parent::setUp();

        $this->difficulty1 = Difficulty::factory()->create(['level' => 10000]);
        $this->difficulty2 = Difficulty::factory()->create(['level' => 20000]);
        $this->plantDefinition1 = PlantDefinition::factory()->create(['difficulty_id' => $this->difficulty1->id]);
        $this->plantDefinition2 = PlantDefinition::factory()->create(['difficulty_id' => $this->difficulty2->id]);
    }

    public function test_max_level(): void
    {
        $this->assertEquals(20000, Difficulty::maxLevel());
    }

    public function test_count_level(): void
    {
        $this->assertEquals(7, Difficulty::countDistinct('level'));
    }

    public function test_plant_definition_relationship() : void
    {
        $this->assertTrue($this->difficulty1->plant_definition->contains($this->plantDefinition1));
        $this->assertTrue($this->difficulty2->plant_definition->contains($this->plantDefinition2));
    }
}