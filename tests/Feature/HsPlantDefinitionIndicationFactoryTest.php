<?php

namespace Tests\Feature;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsPlantDefinitionIndication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsPlantDefinitionIndicationFactoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_create_hs_plant_definition_indication()
    {
        $indication = HsPlantDefinitionIndication::factory()->create();

        $this->assertModelExists($indication);
    }

    /** @test */
    public function it_belongs_to_hs_plant_definition()
    {
        $plantDefinition = HsPlantDefinition::factory()->create();
        $indication = HsPlantDefinitionIndication::factory()->create([
            'hs_plant_definition_id' => $plantDefinition->id,
        ]);

        $this->assertInstanceOf(HsPlantDefinition::class, $indication->hs_plant_definition);
        $this->assertEquals($plantDefinition->id, $indication->hs_plant_definition->id);
    }

    /** @test */
    public function it_can_update_hs_plant_definition_indication()
    {
        $indication = HsPlantDefinitionIndication::factory()->create([
            'message' => 'Initial Message',
            'step' => 'before',
        ]);

        $updatedData = [
            'message' => 'Updated Message',
            'step' => 'after',
        ];

        $indication->update($updatedData);

        $this->assertDatabaseHas('hs_plant_definition_indications', $updatedData);
    }

    /** @test */
    public function it_can_delete_hs_plant_definition_indication()
    {
        $indication = HsPlantDefinitionIndication::factory()->create();

        $indicationId = $indication->id;
        $indication->delete();

        $this->assertDatabaseMissing('hs_plant_definition_indications', [
            'id' => $indicationId,
        ]);
    }
}
