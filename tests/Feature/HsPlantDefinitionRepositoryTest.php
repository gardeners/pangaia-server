<?php

namespace Tests\Unit;

use App\Models\Hs\HsPlantDefinition;
use App\Models\PlantDefinition;
use App\Models\Repositories\Hs\HsPlantDefinitionRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsPlantDefinitionRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_retrieve_hs_plant_definitions_by_plant_definition_id()
    {
        $plantDefinition = PlantDefinition::factory()->create();
        $hsPlantDefinition = HsPlantDefinition::factory()->create(
            ['plant_definitions_id' => $plantDefinition->id]
        );
        $otherPlantDefinition = HsPlantDefinition::factory()->create();

        $repository = new HsPlantDefinitionRepository(new HsPlantDefinition());
        $fetchedDefinitions = $repository->getByPlantDefinitionId($plantDefinition->id);
        $this->assertCount(1, $fetchedDefinitions);
        $this->assertTrue($fetchedDefinitions->first()->is($hsPlantDefinition));
        $this->assertFalse($fetchedDefinitions->first()->is($otherPlantDefinition));
    }
}
