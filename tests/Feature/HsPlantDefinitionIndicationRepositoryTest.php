<?php

namespace Tests\Unit;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsPlantDefinitionIndication;
use App\Models\Repositories\Hs\HsPlantDefinitionIndicationRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HsPlantDefinitionIndicationRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_retrieve_hs_plant_definition_indications_by_plant_definition_id()
    {
        $plantDefinition = HsPlantDefinition::factory()->create();
        $indications = HsPlantDefinitionIndication::factory()->count(3)->create([
            'hs_plant_definition_id' => $plantDefinition->id,
        ]);

        $repository = new HsPlantDefinitionIndicationRepository(new HsPlantDefinitionIndication());
        $fetchedIndications = $repository->getByHsPlantDefinitionId($plantDefinition->id);

        $this->assertCount(3, $fetchedIndications);
        $this->assertInstanceOf(HsPlantDefinitionIndication::class, $fetchedIndications->first());
        $this->assertEquals($indications->pluck('id')->sort()->values(), $fetchedIndications->pluck('id')->sort()->values());
    }
}
