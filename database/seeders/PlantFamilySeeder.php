<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PlantFamily;

class PlantFamilySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/plant-families.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData['families'] as $record) {
            PlantFamily::firstOrCreate([
                "name" => $record['name']
            ]);
        }
    }
}
