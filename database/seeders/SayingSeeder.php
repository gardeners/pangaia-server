<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Saying;

class SayingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/dictons.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData as $key => $record) {
            Saying::firstOrCreate([
                'title' => $record['title'],
                'date_beg' => $record['date_beg'],
                'date_end' => $record['date_end']
            ]);
        }


        $path = __DIR__ . "/data/fete_journees.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData as $key => $record) {
            Saying::firstOrCreate([
                'title' => $record['name'],
                'date_beg' => $record['start_date'],
                'date_end' => $record['end_date']
            ]);
        }
    }
}
