<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call([
            SayingSeeder::class,
            PermissionSeeder::class,
            PlantFamilySeeder::class,
            TaskDefinitionTypeSeeder::class,
            DifficultySeeder::class,
            PlantDefinitionSeeder::class,
            TaskDefinitionSeeder::class,

            HsLevelSeeder::class,

            GardenerDemoSeeder::class,
            GardenerDocusSeeder::class,
            GardenerPtitLuSeeder::class,
            GardenerPaulBaptisteSeeder::class,

            HsPlantDefinitionSeeder::class,
            HsProfileSeeder::class
        ]);
    }
}