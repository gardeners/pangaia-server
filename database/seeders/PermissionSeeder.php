<?php

namespace Database\Seeders;

use Backpack\PermissionManager\app\Models\Role;
use Backpack\PermissionManager\app\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class PermissionSeeder extends Seeder
{

    const ROLES = ['create', 'update', 'delete', 'list', 'show'];
    const ROLES_AND_PERMISSION = [
        User::ROLE_DEMO => [],
        User::ROLE_USER => ['garden', 'plant', 'task', 'zone'],
        User::ROLE_BOTANIST => ['plant_family', 'plant_definition', 'saying', 'task_definition', 'task_definition_type'],
        User::ROLE_ADMIN => ['permission', 'role', 'user']
    ];
    /**
     * Run the database seeds.
     */

    private function createPermission($name): Permission
    {
        $p = Permission::firstOrCreate([
            "name" => $name,
            "guard_name" => 'web'
        ]);
        return $p;
    }
    private function createRole($name): Role
    {
        $r = Role::firstOrCreate([
            "name" => $name,
            "guard_name" => 'web'
        ]);
        return $r;
    }


    public function run(): void
    {
        foreach (self::ROLES_AND_PERMISSION as $role => $permissions) {
            $r = $this->createRole($role);
            foreach ($permissions as $o) {
                foreach (self::ROLES as $role) {
                    $p = $this->createPermission($o . '.' . $role);
                    $r->permissions()->syncWithoutDetaching($p);
                }
            }
        }



    }
}