<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ZipcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $zipcodes = ['35000', '35340', '75000'];
        foreach ($zipcodes as $zipcode) {
            DB::insert('insert into zip_codes (code) values (?)', [$zipcode]);
        }
    }
}