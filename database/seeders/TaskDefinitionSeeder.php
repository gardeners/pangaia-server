<?php

namespace Database\Seeders;

use App\Models\AlternatePlantName;
use Illuminate\Database\Seeder;
use App\Models\TaskDefinition;
use App\Models\PlantDefinition;
use App\Models\TaskDefinitionType;

class TaskDefinitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $path = __DIR__ . "/data/task-definitions.json";
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        foreach ($jsonData['task_definitions'] as $record) {
            $td = TaskDefinition::firstOrCreate([
                "task_definition_type_id" => TaskDefinitionType::where('name', $record['type'])->first()->id,
                "name" => $record['name'],
                "date_beg" => $record['date_beg'],
                "date_end" => $record['date_end'],
                "description" => $record['description']
            ]);

            foreach ($jsonData['relations'] as $relation) {
                if ($relation['task_definition'] == $record['name']) {
                    $plant_name_id = AlternatePlantName::where('name', $relation['plant'])->first()->id;
                    $plant_def_id = PlantDefinition::whereHas('alternatePlantNames', function ($query) use ($plant_name_id) {
                        $query->where('alternate_plant_name_id', $plant_name_id)
                              ->where('is_most_common_plant_name', true);
                    })->first()->id;
                    $td->plant_definition_id = $plant_def_id;
                    $td->save();
                }
            }
        }
    }
}
