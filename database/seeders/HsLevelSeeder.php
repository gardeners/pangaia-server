<?php

namespace Database\Seeders;

use App\Models\Hs\HsProfile;
use App\Models\Hs\HsLevel;
use Illuminate\Database\Seeder;

class HsLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $levels = [
            0 => "Lombric",
            2 => "Souris",
            4 => "Lézard",
            7 => "Hérisson",
            11 => "Coccinelle",
            16 => "Pie",
            24 => "Papillon",
            42 => "Loutre"
        ];
        foreach ($levels as $xp => $name) {
            HsLevel::firstOrCreate([
                "name" => $name,
                "min_experience" => $xp
            ]);
        }


    }
}