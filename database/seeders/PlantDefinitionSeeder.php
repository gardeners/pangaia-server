<?php

namespace Database\Seeders;

use App\Models\Pictures;
use App\Models\AlternatePlantName;
use Illuminate\Database\Seeder;
use App\Models\PlantDefinition;
use App\Models\PlantDefinitionRequirements;
use App\Models\PlantDefinitionHeights;
use App\Models\PlantDefinitionWidenesses;
use App\Models\PlantFamily;

class PlantDefinitionSeeder extends Seeder
{
  /**
   * Run the database seeds for PlantDefinition and associated tables.
   */
  public function run(): void
  {
    $path = __DIR__ . "/data/plant-definitions.json";
    $jsonString = file_get_contents($path);
    $jsonData = json_decode($jsonString, true);
    foreach ($jsonData['plants'] as $record) {
      if (array_key_exists('requirements', $record)) {
        foreach ($record['requirements'] as $requirement) {
          $requirements = PlantDefinitionRequirements::firstOrCreate([
            'exposition_level' => $requirement['exposition_level'],
            'exposition_label' => $requirement['exposition_label'],
            'humidity_level' => $requirement['humidity_level'],
            'humidity_label' => $requirement['humidity_label'],
          ]);
        }
      }
      if (array_key_exists('max_height', $record)) {
        $height = PlantDefinitionHeights::firstOrCreate([
          'min_height' => $record['min_height'],
          'max_height' => $record['max_height'],
        ]);
      }
      if (array_key_exists('max_width', $record)) {
        $wideness = PlantDefinitionWidenesses::firstOrCreate([
          'min_width' => $record['min_width'],
          'max_width' => $record['max_width'],
        ]);
      }
      $plantFamily = PlantFamily::firstOrCreate([
        'name' => $record['family'],
      ]);

      $plantDefinition = PlantDefinition::firstOrCreate([
        'description' => $record['description'],
        'latin_name' => $record['latin_name'],
        'difficulty_id' => array_key_exists('difficulty_id', $record) ? $record['difficulty_id'] : null,
        'requirement_id' => $requirements ? $requirements->id : null,
        'height_id' => $height ? $height->id : null,
        'wideness_id' => $wideness ? $wideness->id : null,
        'family_id' => $plantFamily->id,
      ]);

      if (array_key_exists('photo_url', $record)) {
        foreach ($record['photo_url'] as $photo) {
          $picture = Pictures::firstOrCreate([
            'picture_name' => $photo['name'],
            'is_main' => $photo['is_main'],
            'imageable_id' => $plantDefinition->id,
            'imageable_type' => 'App\Models\PlantDefinition',
          ]);
        }
      }
      $names = is_array($record['names']) ? $record['names'] : [$record['names']];
      foreach ($names as $index => $name) {
        $alternatePlantName = AlternatePlantName::firstOrCreate([
          'name' => $name,
        ]);

        // Attach the alternate plant name to the plant definition  
        $plantDefinition->alternatePlantNames()->attach($alternatePlantName->id, ['is_most_common_plant_name' => $index == 0]);
      }
    }
  }
}
