<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Team;
use App\Models\Zone;
use App\Models\Plant;
use App\Models\Task;
use Backpack\PermissionManager\app\Models\Role;

class GardenerPtitLuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $model = [
            "name" => "ptitlu",
            "email" => 'ptitlu@pangaia.app'
        ];
        $user = User::where($model)->first();
        if (empty($user)) {
            $model["password"] = Hash::make('Passwordpassword42');
            $user = User::create($model);
        }
        $user->assignRole(Role::findByName(User::ROLE_USER, 'web'));
        $user->assignRole(Role::findByName(User::ROLE_BOTANIST, 'web'));


        // Create the garden
        $user->ownedTeams()->save(
            Team::unguarded(function () use ($user) {
                return Team::firstOrCreate([
                    'user_id' => $user->id,
                    'name' => "Jardin de " . explode(' ', $user->name, 2)[0],
                    'personal_team' => true
                ]);
            })
        );

        $zone = Zone::firstOrCreate([
            "garden_id" => $user->currentTeam()->first()->id,
            "name" => "P'tit Lu's Tomato Zone",
            "width" => 50,
            "height" => 100,
            "position_top" => 150,
            "position_left" => 250

        ]);
        $verger = Zone::firstOrCreate([
            "garden_id" => $user->currentTeam()->first()->id,
            "name" => "P'tit Lu's Peach Zone",
            "width" => 100,
            "height" => 50,
            "position_top" => 250,
            "position_left" => 150

        ]);
        $fraisiers = Zone::firstOrCreate([
            "garden_id" => $user->currentTeam()->first()->id,
            "name" => "P'tit Lu's Strawberry Zone",
            "width" => 300,
            "height" => 25,
            "position_top" => 75,
            "position_left" => 150

        ]);
        $tomato = Plant::firstOrCreate([
            "zone_id" => $zone->id,
            "name" => "Tomate cerise"
        ]);
        $peach = Plant::firstOrCreate([
            "zone_id" => $verger->id,
            "name" => "Pêcher malade"
        ]);
        $strawberry = Plant::firstOrCreate([
            "zone_id" => $fraisiers->id,
            "name" => "Fraise blanche"
        ]);

        foreach ([$strawberry, $tomato, $peach] as $plant) {
            Task::firstOrCreate([
                "plant_id" => $plant->id,
                "name" => "Arroser les {$plant->name}",
                "description" => "L'eau ça mouille les {$plant->name}"
            ]);
        }
    }
}