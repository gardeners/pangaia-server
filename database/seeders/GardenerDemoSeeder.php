<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Team;
use App\Models\Zone;
use App\Models\Plant;
use App\Models\Task;
use Backpack\PermissionManager\app\Models\Role;

class GardenerDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $model = [
            "name" => "demo",
            "email" => 'demo@pangaia.app'
        ];
        $user = User::where($model)->first();
        if (empty ($user)) {
            $model["password"] = Hash::make('Pangaia est une super application de jardinage');
            $user = User::create($model);
        }
        $user->assignRole(Role::findByName('Demo', 'web'));


        // Create the garden
        $user->ownedTeams()->save(
            Team::unguarded(function () use ($user) {
                return Team::firstOrCreate([
                    'user_id' => $user->id,
                    'name' => "Jardin de " . explode(' ', $user->name, 2)[0],
                    'personal_team' => true
                ]);
            })
        );


        // Create the zones
        Zone::firstOrCreate([
            "garden_id" => $user->currentTeam()->first()->id,
            "name" => "Potager",
            "width" => 50,
            "height" => 100,
            "position_top" => 150,
            "position_left" => 250
        ]);
    }
}