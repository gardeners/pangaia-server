<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plant_definition_heights', function (Blueprint $table) {
            $table->id();
            $table->integer('min_height');
            $table->integer('max_height');
            $table->timestamps();
        });

        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->foreignId('height_id')->nullable()->constrained('plant_definition_heights')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plant_definition_heights');
        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->dropForeign(['height_id']);
            $table->dropColumn('height_id');
        });
    }
};
