<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plant_definition_requirements', function (Blueprint $table) {
            $table->id();
            $table->integer('exposition_level');
            $table->string('exposition_label');
            $table->integer('humidity_level');
            $table->string('humidity_label');
            $table->timestamps();
        });

        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->foreignId('requirement_id')->nullable()->constrained('plant_definition_requirements')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plant_definition_requirements');
        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->dropForeign(['requirement_id']);
            $table->dropColumn('requirement_id');
        });
    }
};
