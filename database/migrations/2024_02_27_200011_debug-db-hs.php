<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('hs_plant_definition_indications', function (Blueprint $table) {
        $table->foreignId('hs_plant_definition_id')->constrained('hs_plant_definitions')->onDelete('cascade');
        });

        Schema::table('hs_plant_definitions', function (Blueprint $table) {
        $table->foreignId('plant_definitions_id')->constrained('plant_definitions')->onDelete('cascade');
        });

        Schema::table('hs_profile_plants', function (Blueprint $table) {
        $table->foreignId('hs_plant_definitions_id')->constrained('hs_plant_definitions')->onDelete('cascade');
        $table->foreignId('hs_profile_id')->constrained('hs_profiles')->onDelete('cascade');
        });

        Schema::table('hs_profiles', function (Blueprint $table) {
        $table->foreignId('hs_level_id')->constrained('hs_levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('hs_profile_plants', function (Blueprint $table) {
            $table->dropForeign(['hs_plant_definitions_id']);
            $table->dropForeign(['hs_profile_id']);
        });

        Schema::table('hs_profiles', function (Blueprint $table) {
            $table->dropForeign(['hs_level_id']);
        });

        Schema::table('hs_plant_definitions', function (Blueprint $table) {
            $table->dropForeign(['plant_definitions_id']);
        });

        Schema::table('plants', function (Blueprint $table) {
            $table->dropForeign(['plant_definitions_id']);
        });

        Schema::table('hs_plant_definition_indications', function (Blueprint $table) {
            $table->dropForeign(['hs_plant_definitions_id']);
        });
    }
};
