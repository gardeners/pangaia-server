<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->id();
            $table->integer('imageable_id');
            $table->string('imageable_type');
            $table->string('picture_name');
            $table->boolean('is_main')->default(false);
            $table->timestamps();
        });

        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->dropColumn('photo_url');
        }); 
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pictures');
        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->string('photo_url')->nullable();
        });
    }
};
