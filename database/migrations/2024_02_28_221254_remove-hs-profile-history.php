<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('hs_profile_historys');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::create('hs_profile_historys', function ($table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('hs_history_id')->constrained('hs_historys');
        });
    }
};
