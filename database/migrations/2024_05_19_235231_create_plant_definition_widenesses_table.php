<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plant_definition_widenesses', function (Blueprint $table) {
            $table->id();
            $table->integer('min_width');
            $table->integer('max_width');
            $table->timestamps();
        });

        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->foreignId('wideness_id')->nullable()->constrained('plant_definition_widenesses')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plant_definition_widenesses');
        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->dropForeign(['wideness_id']);
            $table->dropColumn('wideness_id');
        });
    }
};
