<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('zip_codes', function (Blueprint $table) {
            $table->id();
            $table->integer('code', false, true)->unique();
            $table->string('name')->nullable();
            $table->float('longitude', 4)->nullable();
            $table->float('latitude', 4)->nullable();
            $table->integer("sea_level")->nullable();
            $table->integer("grnd_level")->nullable();
            $table->timestamps();
        });
        Schema::create('open_weather_map_codes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string("description");
        });
        Schema::create('weathers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('zip_code_id')->constrained('zip_codes')->onDelete('cascade');
            $table->foreignId('open_weather_map_id')->constrained('open_weather_map_codes')->onDelete('cascade');
            $table->float("temp", 5, 2);
            $table->float("feels_like", 5, 2);
            $table->integer("pressure");
            $table->integer("humidity");
            $table->float("wind_speed", 5, 2);
            $table->float("wind_deg", 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('weathers');
        Schema::dropIfExists('open_weather_map_codes');
        Schema::dropIfExists('zip_codess');
    }
};
