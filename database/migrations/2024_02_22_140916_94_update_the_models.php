<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('zones', function (Blueprint $table) {
            $table->unsignedBigInteger('garden_id')->nullable()->change();
            $table->foreign('garden_id')->references('id')->on('gardens')->nullOnDelete();
        });
        Schema::table('plants', function ($table) {
            $table->dropColumn('cultivar');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('zip_code');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('zones', function (Blueprint $table) {
            $table->dropForeign(['garden_id']);
        });
        Schema::create('plants', function ($table) {
            $table->string('cultivar')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('zip_code');
        });

    }
};
