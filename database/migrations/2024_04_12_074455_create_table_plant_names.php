<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('alternate_plant_names', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('plant_def_has_alt_name', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_most_common_plant_name');
            $table->foreignId('alternate_plant_name_id')->constrained('alternate_plant_names')->onDelete('cascade');
            $table->foreignId('plant_definition_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    { 
        Schema::dropIfExists('plant_def_has_alt_name');
        Schema::dropIfExists('alternate_plant_names');
    }
};