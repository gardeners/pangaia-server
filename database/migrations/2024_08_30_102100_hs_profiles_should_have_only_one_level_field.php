<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('hs_profiles', function (Blueprint $table) {
            $table->dropColumn('level');

            $table->dropForeign(['hs_level_id']);
            $table->dropColumn('hs_level_id');
        });
        Schema::table('hs_profiles', function (Blueprint $table) {
            $table->foreignId('level')->constrained('hs_levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('hs_profiles', function (Blueprint $table) {
            $table->dropForeign(['level']);
            $table->foreignId('hs_level_id')->constrained('hs_levels')->onDelete('cascade');
        });
    }
};
