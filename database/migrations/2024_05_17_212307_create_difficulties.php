<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('difficulties', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name')->unique()->comment('The name of the difficulty level');
            $table->integer('level')->unique()->comment('The level of the difficulty');
        });
    
        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->foreignId('difficulty_id')->nullable()->constrained('difficulties')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('difficulties');

        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->dropForeign(['difficulty_id']);
            $table->dropColumn('difficulty_id');
        });
    }
};
