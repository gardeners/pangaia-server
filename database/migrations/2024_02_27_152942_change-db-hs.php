<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->decimal('latitude')->nullable();
            $table->decimal('longitude')->nullable();
        });

        Schema::table('plant_definitions', function (Blueprint $table) {
            $table->string('photo_url')->nullable();
        });

        Schema::create('hs_plant_definition_indications', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('message');
            $table->enum('step', ['before', 'after']);
        });

        Schema::create('hs_plant_definitions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('level');
        });

        Schema::create('hs_profile_plants', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('found_date');
        });

    Schema::create('hs_profiles', function (Blueprint $table) {
        $table->id();
        $table->timestamps();
        $table->integer('min_experience');
        $table->string('profile_picture');
    });

        Schema::create('hs_levels', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->integer('min_experience');
        });

        Schema::create('hs_profile_historys', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });

        Schema::create('hs_historys', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('user_id')->constrained();
        });

        Schema::table('hs_profile_historys', function (Blueprint $table) {
            $table->foreignId('hs_history_id')->constrained('hs_historys');
        });

    }

public function down(): void
{
    Schema::table('hs_profile_historys', function (Blueprint $table) {
        $table->dropForeign(['hs_history_id']);
    });
    Schema::table('hs_historys', function (Blueprint $table) {
        $table->dropForeign(['user_id']);
    });
    Schema::dropIfExists('hs_profile_historys');
    Schema::dropIfExists('hs_historys');
    Schema::dropIfExists('hs_levels');
    Schema::dropIfExists('hs_profiles');
    Schema::dropIfExists('hs_profile_plants');
    Schema::dropIfExists('hs_plant_definitions');
    Schema::dropIfExists('hs_plant_definition_indications');
    Schema::table('plants', function (Blueprint $table) {
        $table->dropColumn('latitude');
        $table->dropColumn('longitude');
    });
    Schema::table('plant_definitions', function (Blueprint $table) {
        $table->dropColumn('photo_url');
    });
}
};
