<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\PlantDefinition;
use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Plant>
 */
class ZoneFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'width' => fake()->numberBetween(10, 1000),
            'height' => fake()->numberBetween(10, 1000),
            'position_top' => fake()->numberBetween(0, 100),
            'position_left' => fake()->numberBetween(0, 100),
            'garden_id' => Team::factory()->create()->id
        ];
    }
}
