<?php

namespace Database\Factories;

use App\Models\Difficulty;
use App\Models\PlantDefinition;
use App\Models\PlantDefinitionHeights;
use App\Models\PlantDefinitionRequirements;
use App\Models\PlantDefinitionWidenesses;
use App\Models\PlantFamily;
use App\Models\Repositories\PlantDefinitionRequirementsRepository;
use App\Models\Repositories\PlantFamilyRepository;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlantDefinition>
 */
class PlantDefinitionFactory extends Factory
{
    /**
     * Model linked to factory
     *
     * @var string
     */
    protected $model = PlantDefinition::class;


    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'description' => fake()->paragraph(),
            'latin_name' => fake()->sentence(),
            'difficulty_id' => Difficulty::inRandomOrder()->first()->id,
            'requirement_id' => PlantDefinitionRequirements::factory()->create()->id,
            'height_id' => PlantDefinitionHeights::factory()->create()->id,
            'wideness_id' => PlantDefinitionWidenesses::factory()->create()->id,
            'family_id' => PlantFamily::factory()->create()->id,
        ];
    }
}
