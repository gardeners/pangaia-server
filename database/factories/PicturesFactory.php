<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pictures>
 */
class PicturesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'picture_name' => $this->faker->word,
            'imageable_id' => $this->faker->randomNumber(1, 13),
            'imageable_type' => $this->faker->word,
        ];
    }
}
