<?php

namespace Database\Factories\Hs;

use App\Models\Hs\HsProfilePlant;
use Illuminate\Database\Eloquent\Factories\Factory;

class HsProfilePlantFactory extends Factory
{
    protected $model = HsProfilePlant::class;

    public function definition()
    {
        return [
            'found_date' => $this->faker->date(),
            'hs_plant_definitions_id' => function () {
                return \App\Models\Hs\HsPlantDefinition::factory()->create()->id;
            },
            'hs_profile_id' => function () {
                return \App\Models\Hs\HsProfile::factory()->create()->id;
            },
        ];
    }
}
