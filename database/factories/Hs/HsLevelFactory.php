<?php

namespace Database\Factories\Hs;

use App\Models\Hs\HsLevel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\HsLevel>
 */
class HsLevelFactory extends Factory
{
    protected $model = HsLevel::class;

    public function definition(): array
    {
        static $min_experience = 0;
        return [
            'name' => $this->faker->unique()->jobTitle,
            'min_experience' => $min_experience++,
        ];
    }
}
