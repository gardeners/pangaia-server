<?php

namespace Database\Factories\Hs;

use App\Models\Hs\HsPlantDefinition;
use App\Models\Hs\HsPlantDefinitionIndication;
use Illuminate\Database\Eloquent\Factories\Factory;

class HsPlantDefinitionIndicationFactory extends Factory
{
    protected $model = HsPlantDefinitionIndication::class;

    public function definition(): array
    {
        return [
            'message' => $this->faker->sentence,
            'step' => $this->faker->randomElement(['before', 'after']),
            'hs_plant_definition_id' => HsPlantDefinition::factory(),
        ];
    }

    public function withHsPlantDefinition()
    {
        return $this->for(HsPlantDefinition::class, 'hs_plant_definition');
    }

}
