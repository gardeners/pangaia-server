<?php

namespace Database\Factories\Hs;

use App\Models\Hs\{HsProfile, HsLevel};
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class HsProfileFactory extends Factory
{
    protected $model = HsProfile::class;

    public function definition(): array
    {
        return [
            'user_id' => User::inRandomOrder()->first()->id,
            'name' => $this->faker->name(),
            'experience' => $this->faker->numberBetween(1, 100),
            'profile_picture' => $this->faker->imageUrl(640, 480, 'people'),
            'level' => HsLevel::factory()->create()->id,
        ];
    }
}
