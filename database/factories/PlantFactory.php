<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\PlantDefinition;
use App\Models\Zone;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Plant>
 */
class PlantFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        // FIXME: plant definition should not be set this way
        return [
            'name' => fake()->word(),
            'description' => fake()->sentence(),
            'plant_definition_id' => PlantDefinition::first()->id,
            'zone_id' => Zone::factory()->create()->id,
        ];
    }
}
