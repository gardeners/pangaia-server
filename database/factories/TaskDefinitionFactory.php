<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlantFamily>
 */
class TaskDefinitionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->sentence(),
            // "task_definition_type" => TaskDefinitionType::where('name', 'Taille')->first()->id,
            "description" => fake()->paragraph(),
            'date_beg' => fake()->dateTimeInInterval('-1 week', '0 days')->format('Y-m-d'),
            'date_end' => fake()->dateTimeInInterval('1 days', '1 week')->format('Y-m-d'),
        ];
    }
}
