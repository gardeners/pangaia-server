<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Saying;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Saying>
 */
class SayingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->sentence(),
            'date_beg' => fake()->dateTimeInInterval('-1 week', '0 days')->format('Y-m-d'),
            'date_end' => fake()->dateTimeInInterval('1 days', '1 week')->format('Y-m-d'),
        ];
    }

    public function swapDates(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'date_beg' => fake()->dateTimeInInterval('1 days', '1 week')->format('Y-m-d'),
                'date_end' => fake()->dateTimeInInterval('-1 week', '0 days')->format('Y-m-d'),
            ];
        });
    }
}
