@echo Stopping MySQL... && (sc queryex wampmysqld64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampmysqld64 && echo MySQL stopped.) || echo MySQL was already stopped.
@echo Stopping Apache... && (sc queryex wampapache64 | find "STATE" | find /i "RUNNING" >nul && sc stop wampapache64 && echo Apache stopped.) || echo Apache was already stopped.

