<?php

namespace App\Http\Controllers;


use App\Http\Controllers\PlantDefinitionController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ZipCodeController;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Homepage', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('home');


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [DashBoardController::class, 'index'])->name('dashboard');
    Route::put('/zones/{id}', [DashBoardController::class, 'update']);
    Route::get('/games/hs', [HsController::class, 'index'])->name('hs_home');
    Route::get('/library', [PlantDefinitionController::class, 'index'])->name('plant_definitions.show');
    Route::get('/plants/{id}', [PlantDefinitionController::class, 'show'])->name('single_plant_definiton.show');
});

/**
 * Hide and seek module
 */
Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/games/hs', [HsController::class, 'index'])->name('hs_home');
    Route::get('/games/hs/approve', [HsController::class, "requestForParentApproval"])->name('hs_approval');
    Route::post('/games/hs/plantFound', [HsController::class, 'approved'])->name('hs_plant_found');
    Route::get('/games/hs/debrief', [HsController::class, 'debrief'])->name('hs_debrief');
    Route::get('/games/hs/reinit/{profile}', [HsController::class, 'reinit'])->name('hs_reinit');
    Route::get('/games/hs/{profile}', [HsController::class, 'startGame'])->name('hs_start_a_game');
});


Route::get('/generate-password', [PasswordGeneratorController::class, 'generate']);
Route::post('/zipcode', [ZipCodeController::class, 'store'])->name('zipcode');
Route::get('/weather/{zipcode}', [ZipCodeController::class, 'getWeather'])->name('weather_for_zipcode');
