<?php

use App\Http\Controllers\Api\Admin\AdminGardenApiController;
use App\Http\Controllers\Api\GardenApiController;
use App\Http\Controllers\Api\InventoryApiController;
use App\Models\Garden;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|   
| https://laravel.com/docs/10.x/routing
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// https://blog.quickadminpanel.com/laravel-api-documentation-with-openapiswagger/
Route::group([
    'prefix' => 'v1',
    'as' => 'api.',
    'namespace' => 'App\Http\Controllers\Api',
    'middleware' => ['auth:sanctum']
], function () {
    Route::apiResource('plant', 'PlantApiController');
    Route::apiResource('plant-definition', 'PlantDefinitionApiController');
    Route::apiResource('plant-family', 'PlantFamilyApiController');
    Route::apiResource('saying', 'SayingApiController');
    Route::apiResource('task', 'TaskApiController');
    Route::apiResource('task-definition', 'TaskDefinitionApiController');
    Route::apiResource('task-definition-type', 'TaskDefinitionTypeApiController');
    Route::apiResource('zone', 'ZoneApiController');
    Route::apiResource('/user/{user}/inventory', InventoryApiController::class);
    Route::apiResource('/garden', GardenApiController::class);
});

// Admin routes
Route::group([
    'prefix' => 'v1/admin',
    'as' => 'api.admin.',
    'namespace' => 'App\Http\Controllers\Api\Admin',
    'middleware' => ['auth:sanctum', 'auth.admin']
], function () {
    Route::apiResource('/garden', AdminGardenApiController::class);
});

Route::fallback(function () {
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact contact@pangaia.app'
    ], Response::HTTP_NOT_FOUND);
});