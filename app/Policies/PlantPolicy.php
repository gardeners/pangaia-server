<?php

namespace App\Policies;

use App\Models\Plant;
use App\Models\User;

class PlantPolicy
{
    private function canAccess(User $user, Plant $plant): bool
    {
        return $user->belongsToTeam($plant->zone->garden) || $user->hasRole(User::ROLE_ADMIN);
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Plant $plant): bool
    {
        return $this->canAccess($user, $plant);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Plant $plant): bool
    {
        return $this->canAccess($user, $plant);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Plant $plant): bool
    {
        return $this->canAccess($user, $plant);
    }

    /**
     * Determine whether the user can restore the model.
     */
    // public function restore(User $user, Plant $Plant): bool
    // {
    //     //
    // }

    /**
     * Determine whether the user can permanently delete the model.
     */
    // public function forceDelete(User $user, Plant $Plant): bool
    // {
    //     //
    // }
}
