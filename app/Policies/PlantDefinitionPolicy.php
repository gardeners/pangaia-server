<?php

namespace App\Policies;

use App\Models\PlantDefinition;
use App\Models\User;

class PlantDefinitionPolicy
{
    private function canAccess(User $user): bool
    {
        return $user->hasRole([User::ROLE_ADMIN, User::ROLE_USER, User::ROLE_BOTANIST]);
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $this->canAccess($user);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, PlantDefinition $plantDefinition): bool
    {
        return $this->canAccess($user, $plantDefinition);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, PlantDefinition $plantDefinition): bool
    {
        return $this->canAccess($user, $plantDefinition);
    }
}
