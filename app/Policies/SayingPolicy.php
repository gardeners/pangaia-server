<?php

namespace App\Policies;

use App\Models\Saying;
use App\Models\User;

class SayingPolicy
{
    private function canAccess(User $user): bool
    {
        return $user->hasRole([User::ROLE_ADMIN, User::ROLE_BOTANIST]);
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $this->canAccess($user);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Saying $saying): bool
    {
        return $this->canAccess($user, $saying);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Saying $saying): bool
    {
        return $this->canAccess($user, $saying);
    }
}
