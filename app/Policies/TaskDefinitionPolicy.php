<?php

namespace App\Policies;

use App\Models\TaskDefinition;
use App\Models\User;

class TaskDefinitionPolicy
{
    /**
     * Create a new policy instance.
     */
    private function canAccess(User $user): bool
    {
        return $user->hasRole([User::ROLE_ADMIN, User::ROLE_BOTANIST]);
    }

    /**
     * Determine whether the user can view the model.
     */
    public function create(User $user): bool
    {
        return $this->canAccess($user);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, TaskDefinition $taskDefinition): bool
    {
        return $this->canAccess($user);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, TaskDefinition $taskDefinition): bool
    {
        return $this->canAccess($user);
    }
}
