<?php

namespace App\Policies;

use App\Models\Task;
use App\Models\User;

class TaskPolicy
{
    /**
     * Create a new policy instance.
     */
    private function canAccess(User $user, Task $task): bool
    {
        return (!empty($task->zone) ? $user->belongsToTeam($task->zone->garden) : false) ||
            (!empty($task->plant) ? $user->belongsToTeam($task->plant->zone->garden) : false) ||
            $user->hasRole(User::ROLE_ADMIN);
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Task $task): bool
    {
        return $this->canAccess($user, $task);
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Task $task): bool
    {
        return $this->canAccess($user, $task);
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Task $task): bool
    {
        return $this->canAccess($user, $task);
    }
}
