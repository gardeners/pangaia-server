<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class LogRoute
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        Log::info('Route called', [
            'method' => $request->getMethod(),
            'url' => $request->getPathInfo(),
            'status' => $response->status(),
            'ip' => $request->ip(),
            'referer' => $request->headers->get('referer'),
            'query' => $request->query(),
        ]);

        return $response;
    }
}
