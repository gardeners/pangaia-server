<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\InventoryRequest;
use App\Http\Resources\InventoryPlantDefinitionResource;
use App\Models\User;
use App\Models\PlantDefinition;
use App\Models\Repositories\InventoryRepository;

class InventoryApiController extends Controller
{
   protected $inventoryRepository;

   public function __construct(InventoryRepository $inventoryRepository)
   {
      $this->inventoryRepository = $inventoryRepository;
   }

   /**
    * @OA\Get(
    *    path="/api/v1/user/{user_id}/inventory",
    *    operationId="listUserInventory",
    *    tags={"Inventory"},
    *    summary="Get the inventory of a user",
    *    description="Returns the inventory of a user",
    *    @OA\Parameter(
    *      name="user_id",
    *      in="path",
    *      required=true,
    *      @OA\Schema(
    *          type="integer"
    *      )
    *    ),
    *    @OA\Response(
    *      response=200,
    *      description="Successful operation",
    *      @OA\JsonContent(ref="#/components/schemas/InventoryResource")
    *   ),
    *   security={{ "bearerAuth": {} }},
    * 
    *  @OA\Response(
    *    response=401,
    *    description="Unauthenticated"
    *  ),
    *  @OA\Response(
    *    response=403,
    *    description="Forbidden"
    *  ),
    *  @OA\Response(
    *    response=404,
    *    description="Resource Not Found"
    *  )
    * )
    */
   public function index($user_id)
   {
      $user = User::find($user_id);
      $this->authorize('access', $user);

      $inventory = $this->inventoryRepository->inventory($user_id);


      return response()->json($inventory);
   }


   /**
    * @OA\Post(
    *   path="/api/v1/user/{user_id}/inventory",
    *   operationId="storeUserInventory",
    *   tags={"Inventory"},
    *   summary="Add a plant in the user's inventory",
    *   summary="Add a plant in the user's inventory",
    *   description="Returns the plant added in the inventory of a user",
    *   security={{ "bearerAuth": {} }},
    * 
    *   @OA\Parameter(
    *     name="user_id",
    *     in="path",
    *     required=true,
    *     @OA\Schema(
    *       type="integer"
    *     )
    *   ),
    *  
    *   @OA\RequestBody(
    *    required=true,
    *    @OA\JsonContent(
    *      required={"plant_definition_id", "quantity"},
    *      @OA\Property(property="plant_definition_id", type="integer"),
    *      @OA\Property(property="quantity", type="integer")
    *    )
    *   ),
    *
    *   @OA\Response(
    *     response=200,
    *     description="Plant added to inventory",
    *     @OA\JsonContent(
    *       type="object",
    *       @OA\Schema(ref="#/components/schemas/InventoryPlantDefinitionResource"),
    *       example={"id": 1, "name": "Plant 1", "mainPicture": "plant1.jpg", "quantity": 1, "updated_at": "2021-09-01T00:00:00Z", "created_at": "2021-09-01T00:00:00Z"}
    *     )
    *   ),
    * 
    *   security={{ "bearerAuth": {} }},
    * 
    *   @OA\Response(
    *    response=401,
    *    description="Unauthenticated"
    *   ),
    *   @OA\Response(
    *     response=403,
    *     description="Forbidden"
    *   ),
    *   @OA\Response(
    *     response=404,
    *     description="Resource Not Found"
    *   ),
    *   @OA\Response(
    *     response=422,
    *     description="Plant is already in the inventory"
    *   )
    * )
    */
   public function store(Request $request, $user_id)
   {
      $plant_definition_id = $request->input('plant_definition_id');
      $quantity = $request->input('quantity');
      $user = User::find($user_id);

      $this->authorize('access', $user);

      $postedPlant = $this->inventoryRepository->postInventory($user_id, $plant_definition_id, $quantity);

      return new InventoryPlantDefinitionResource($postedPlant);
   }

   /**
    * @OA\Put(
    *   path="/api/v1/user/{user_id}/inventory/{plant_definition_id}",
    *   operationId="updateUserInventory",
    *   tags={"Inventory"},
    *   summary="Update a plant in the user's inventory",
    *   description="Returns the plant updated in the inventory of a user",
    *   security={{"bearerAuth": {}}},
    * 
    *   @OA\Parameter(
    *     name="user_id",
    *     in="path",
    *     required=true,
    *     @OA\Schema(
    *       type="integer"
    *     )
    *   ),
    *   @OA\Parameter(
    *     name="plant_definition_id",
    *     in="path",
    *     required=true,
    *     @OA\Schema(
    *       type="integer"
    *     )
    *   ),
    *  
    *   @OA\RequestBody(
    *    required=true,
    *    @OA\JsonContent(
    *      type="object",
    *      required={"quantity"},
    *      @OA\Property(property="quantity", type="integer")
    *    )
    *   ),
    *
    *   @OA\Response(
    *     response=200,
    *     description="Plant updated in inventory",
    *     @OA\JsonContent(
    *       type="object",
    *       @OA\Schema(ref="#/components/schemas/InventoryPlantDefinitionResource"),
    *       example={"id": 1, "name": "Plant 1", "mainPicture": "plant1.jpg", "quantity": 1, "updated_at": "2021-09-01T00:00:00Z", "created_at": "2021-09-01T00:00:00Z"}
    *     )
    *   ),
    *   @OA\Response(
    *    response=401,
    *    description="Unauthenticated"
    *   ),
    *   @OA\Response(
    *     response=403,
    *     description="Forbidden"
    *   ),
    *   @OA\Response(
    *     response=404,
    *     description="Resource Not Found"
    *   )
    * )
    */
   public function update(Request $request, $user_id, $id)
   {
      $plant_definition_id = $id;
      $quantity = $request->input('quantity');
      $user = User::findOrFail($user_id);

      $this->authorize('access', $user);

      $this->inventoryRepository->updateInventory($user_id, $plant_definition_id, $quantity);

      $udpatedPlant = $this->inventoryRepository->findPlant($user_id, $plant_definition_id);

      return new InventoryPlantDefinitionResource($udpatedPlant);
   }

   /**
    *  @OA\Delete(
    *    path="/api/v1/user/{user_id}/inventory/{plant_definition_id}",
    *    operationId="deleteUserInventory",
    *    tags={"Inventory"},
    *    summary="Delete a plant in the user's inventory",
    *    description="Returns a message to confirm the deletion of a plant in the inventory of a user",
    *    security={{ "bearerAuth": {} }},
    * 
    *   @OA\Parameter(
    *     name="user_id",
    *     in="path",
    *     required=true,
    *     @OA\Schema(
    *       type="integer"
    *     )
    *   ),
    *   @OA\Parameter(
    *     name="plant_definition_id",
    *     in="path",
    *     required=true,
    *     @OA\Schema(
    *       type="integer"
    *     )
    *   ),
    * 
    *  @OA\Response(
    *     response=200,
    *     description="Plant deleted from inventory",
    *     @OA\JsonContent(
    *       @OA\Property(property="message", type="string")
    *     )  
    *   ),
    *   
    *  @OA\Response(
    *    response=401,
    *    description="Unauthenticated"
    *   ),
    *   @OA\Response(
    *     response=403,
    *     description="Forbidden"
    *   ),
    *   @OA\Response(
    *     response=404,
    *     description="Resource Not Found"
    *   )
    * )
    */
   public function destroy($user_id, $plant_definition_id)
   {
      $user = User::findOrFail($user_id);

      $this->authorize('access', $user);
      $response = $this->inventoryRepository->removePlantDefinition($user_id, $plant_definition_id);

      return response()->json(['message' => "Plant definition {$plant_definition_id} deleted successfully"]);
   }
}
