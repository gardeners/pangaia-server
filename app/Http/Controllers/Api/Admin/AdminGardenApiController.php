<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GardenRequest;
use App\Http\Resources\GardenCollection;
use App\Models\Garden;
use App\Models\Repositories\GardenRepository;
use Exception;
use Illuminate\Support\Facades\Log;

class AdminGardenApiController extends Controller
{
  protected $gardenRepository;

  public function __construct(GardenRepository $gardenRepository)
  {
    $this->gardenRepository = $gardenRepository;
  }

  /**
   *  @OA\Get(
   *    path="/api/v1/admin/garden",
   *    operationId="getAllGardens",
   *    tags={"Admin Garden"},
   *    summary="Get all the gardens on Pangaia app",
   *    description="Returns all the gardens created on Pangaia app",
   *    security={{"bearerAuth": {}}},
   * 
   *    @OA\Response(
   *      response=200,
   *      description="Successfull operation",
   *      @OA\JsonContent(
   *         type="array",
   *         @OA\Items(ref="#/components/schemas/GardenCollection")
   *      )
   *    ),
   *    @OA\Response(
   *        response=401,
   *        description="Unauthenticated"
   *    ),
   *    @OA\Response(
   *        response=403,
   *        description="Forbidden"
   *    ),
   *    @OA\Response(
   *      response=404,
   *      description="Ressource Not found"
   *    )
   *  )
   */
  public function index()
  {
    try {
      $gardens = Garden::all();
      return new GardenCollection($gardens);
    } catch (Exception $e) {
      Log::error("Failed to fetch gardens: " . $e->getMessage());
      return response()->json(['message' => 'Server Error: ' . $e->getMessage()], 500);
    }
  }
}
