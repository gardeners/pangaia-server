<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AlternatePlantName;
use App\Models\PlantDefinition;
use App\Http\Requests\Api\StorePlantDefinitionRequest;
use App\Http\Requests\Api\UpdatePlantDefinitionRequest;
use App\Http\Resources\PlantDefinitionCollection;
use App\Http\Resources\PlantDefinitionResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

/**
 * @OA\Schema(
 *     schema="Family",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *     )
 * )
 *
 * @OA\Schema(
 *     schema="PlantDefinition",
 *     type="object",
 *     @OA\Property(
 *         property="id",
 *         type="integer"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string"
 *     ),
 *     @OA\Property(
 *         property="family",
 *         ref="#/components/schemas/Family"
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string"
 *     )
 * )
 */
class PlantDefinitionApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/plant-definition",
     *      operationId="listPlantDefinitions",
     *      tags={"PlantDefinition"},
     *      summary="List all plants definitions",
     *      description="Returns plant definitions data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="data",
     *                     type="array",
     *                     @OA\Items(ref="#/components/schemas/PlantDefinition")
     *                 ),
     *                 example="data: [{id: 3, name: 'Ail des ours', family: {id: 328, name: 'Liliaceae'}, description: 'Peut etre confondu avec le muguet qui lui est toxique. Pour etre vraiment sûr de ce que vous cueillez, froissez une feuille : l'odeur aillee est si caracteristique que vous ne pouvez vous tromper.\n(EN)'}, links: {self: 'link-value'}]"
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        Log::debug("Listing the plant definitions");
        return new PlantDefinitionCollection(PlantDefinition::with('plantFamily')->paginate());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/plant-definition",
     *      operationId="storePlantDefinition",
     *      tags={"PlantDefinition"},
     *      summary="Store new plant definition",
     *      description="Returns plant definition data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "family"},
     *              @OA\Property(
     *                  property="name", 
     *                  type="array",
     *                  @OA\Items(
     *                         type="string",
     *                         example="Tomate"
     *                  ),
     *               ),
     *              @OA\Property(property="family", type="string", example="Solanaceae"),
     *              @OA\Property(property="description", type="string", example="..."),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StorePlantDefinitionRequest $request)
    {

        // $name = AlternatePlantName::firstOrCreate(['name' => $request->name]);
        if (!Gate::authorize('create-plant-definition')) {
            Log::debug("User " . Auth::id() . " is not allowed to create a plant definition");
            abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
        }
        $plant_definition = PlantDefinition::create([
            'family_id' => $request->input('family_id'),
            'description' => $request->input('description'),
            'latin_name' => $request->input('latin_name'),
        ]);
        foreach ($request->input('name') as $index => $name) {
            $alternate_name = AlternatePlantName::firstOrCreate([
                'name' => $name,
            ]);
            $plant_definition->alternatePlantNames()->attach($alternate_name->id, ['is_most_common_plant_name' => ($index == 0)]); // First record is most common
        }
        return (new PlantDefinitionResource($plant_definition))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/plant-definition/{id}",
     *      operationId="getPlantDefinitionById",
     *      tags={"PlantDefinition"},
     *      summary="Get plant definition information",
     *      description="Returns plant definition data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "name": "Radis", 
     *                      "family": {
     *                         "id": 80,
     *                        "name": "Brassicaceae"
     *                      },
     *                      "description": "..."
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show plant definition $id");
        // abort_if(Gate::denies('project_show'),   Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant_definition = PlantDefinition::findOrFail($id)->load('plantFamily');
        return (new PlantDefinitionResource($plant_definition))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/plant-definition/{id}",
     *      operationId="updatePlantDefinition",
     *      tags={"PlantDefinition"},
     *      summary="Update existing plant definition",
     *      description="Returns updated plant definition data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name", "family"},
     *              @OA\Property(
     *                  property="name", 
     *                  type="array",
     *                  @OA\Items(
     *                         type="string",
     *                         example="Tomate"
     *                  ),
     *               ),
     *              @OA\Property(property="family", type="string", example="Solanaceae"),
     *              @OA\Property(property="description", type="string", example="..."),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdatePlantDefinitionRequest $request)
    {
        Log::debug("Update plant definition $id");
        $plant_definition = PlantDefinition::findOrFail($id);
        if (!Gate::authorize('update', $plant_definition)) {
            Log::debug("User " . Auth::id() . " is not allowed to update plant definition $id");
            abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
        }
        $plant_definition->update([
            'family_id' => $request->input('family_id'),
            'description' => $request->input('description'),
            'latin_name' => $request->input('latin_name'),
        ]);

        $plant_definition->alternatePlantNames()->detach(); // Detach all previous names
        foreach ($request->input('name') as $index => $name) {
            $alternate_name = AlternatePlantName::firstOrCreate([
                'name' => $name,
            ]);
            $plant_definition->alternatePlantNames()->attach($alternate_name->id, ['is_most_common_plant_name' => $index == 0]); // First record is the most common
        }

        return (new PlantDefinitionResource($plant_definition))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/plant-definition/{id}",
     *      operationId="deletePlantDefinition",
     *      tags={"PlantDefinition"},
     *      summary="Delete existing plant definition",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Plant Definition id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(string $id)
    {
        // abort_if(Gate::denies('project_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $plant_definition = PlantDefinition::find($id);
        if (!Gate::authorize('delete', $plant_definition)) {
            Log::debug("User " . Auth::id() . " is not allowed to delete plant definition $id");
            abort(Response::HTTP_FORBIDDEN, '403 Forbidden');
        }
        $http_code = Response::HTTP_NOT_FOUND;
        if (!empty($plant_definition)) {
            $plant_definition->delete();
            $http_code = Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}
