<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PlantRequest;
use App\Http\Controllers\Admin\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\PlantDefinition;

/**
 * Class PlantCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PlantCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->permission_name = 'plant';
        parent::setup();
        CRUD::setModel(\App\Models\Plant::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/plant');
        CRUD::setEntityNameStrings('plant', 'plants');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::column('plant_definition_id');
        CRUD::addColumn([
            'name' => 'plant_definition_id',
            'label' => 'Plant definition',
            'type' => 'closure',
            'function' => function ($entry) {
                return empty ($entry->plant_definition_id) ? '' : PlantDefinition::find($entry->plant_definition_id)->name();
            }
        ]);
        CRUD::column('name');
        CRUD::column('description');
        CRUD::column('zone_id');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PlantRequest::class);

        CRUD::addField([
            // 1-n relationship
            'label' => 'Plant Definition', // Table column heading
            'type' => 'select',
            'name' => 'plant_definition_id', // the column that contains the ID of that connected entity;
            'entity' => 'plant_definition', // the method that defines the relationship in your Model
            'model' => 'App\Models\PlantDefinition',
            'attribute' => 'name', // foreign key attribute that is shown to user
            // OPTIONAL
            // 'limit' => 32, // Limit the number of characters shown
        ]);
        CRUD::field('zone_id')->options((function ($query) {
            return $query->orderBy('name', 'ASC')->get();
        }), );
        CRUD::field('name');
        CRUD::field('description');

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}