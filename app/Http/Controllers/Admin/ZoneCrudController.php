<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ZoneRequest;
use App\Http\Controllers\Admin\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ZoneCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ZoneCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->permission_name = 'zone';
        parent::setup();
        CRUD::setModel(\App\Models\Zone::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/zone');
        CRUD::setEntityNameStrings('zone', 'zones');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name');
        CRUD::column('width');
        CRUD::column('height');
        CRUD::column('position_top');
        CRUD::column('position_left');
        //CRUD::column('garden_id');
        $this->crud->addColumn([
            'name' => 'garden_id',
            // The db column name
            'label' => "Garden",
            // Table column heading
            'type' => 'select',
            'entity' => 'garden',
            // the method that defines the relationship in your Model
            'attribute' => 'name',
            // foreign key attribute that is shown to user
            'model' => "App\Models\Team", // foreign key model
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ZoneRequest::class);

        CRUD::field('name');
        CRUD::field('width');
        CRUD::field('height');
        CRUD::field('position_top');
        CRUD::field('position_left');
        $this->crud->addField([
            'name' => 'garden_id',
            // The db column name
            'label' => "Garden",
            // Table column heading
            'type' => 'select',
            'entity' => 'garden',
            // the method that defines the relationship in your Model
            'attribute' => 'name',
            // foreign key attribute that is shown to user
            'model' => "App\Models\Team", // foreign key model
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Show operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-show
     * @return void
     */
    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }
}