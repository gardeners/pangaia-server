<?php
namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController as BackpackCrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

class CrudController extends BackpackCrudController
{

    protected $permission_name = ''; // Expected value to be a permission configured by the administrator.
    public function setup()
    {
        if (!empty($this->permission_name)) {
            // Manage permissions
            // Based on $permission_name
            if (!backpack_user()->can($this->permission_name . '.create')) {
                CRUD::denyAccess('create');
            }
            // Manage permissions
            // Based on $permission_name
            if (!backpack_user()->can($this->permission_name . '.update')) {
                CRUD::denyAccess('update');
            }
            // Manage permissions
            // Based on $permission_name
            if (!backpack_user()->can($this->permission_name . '.show')) {
                CRUD::denyAccess('show');
            }
            // Manage permissions
            // Based on $permission_name
            if (!backpack_user()->can($this->permission_name . '.list')) {
                CRUD::denyAccess('list');
            }
            // Manage permissions
            // Based on $permission_name
            if (!backpack_user()->can($this->permission_name . '.delete')) {
                CRUD::denyAccess('delete');
            }
        }
    }
}