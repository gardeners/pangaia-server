<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Difficulty;
use App\Models\Repositories\PlantDefinitionRepository;
use App\Models\Repositories\PlantDefinitionRequirementsRepository;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class PlantDefinitionController extends Controller
{
    protected $plantDefinitionRepository;
    protected $plantDefinitionRequirementsRepository;

    public function __construct(PlantDefinitionRepository $plantDefinitionRepository, PlantDefinitionRequirementsRepository $plantDefinitionRequirementsRepository)
    {
      $this->plantDefinitionRepository = $plantDefinitionRepository;
      $this->plantDefinitionRequirementsRepository = $plantDefinitionRequirementsRepository;
    }

    public function index()
    {
      $user = User::findOrFail(Auth::id());
      $is_demo = $user->hasRole(User::ROLE_DEMO); # FIXME : Returning false everytime.
      $plants = $this->plantDefinitionRepository->getAll();
      $difficulty_max = Difficulty::maxLevel();
      return Inertia::render('Library', compact('user', 'plants', 'is_demo', 'difficulty_max'));
    }

    public function show($id)
    {
      $user = User::findOrFail(Auth::id());
      $is_demo = $user->hasRole(User::ROLE_DEMO); # FIXME : Returning false everytime.
      $plant = $this->plantDefinitionRepository->show($id);
      $difficulty_max = Difficulty::maxLevel();
      $exposition_max = $this->plantDefinitionRequirementsRepository->countDistinct('exposition_level');
      $humidity_max = $this->plantDefinitionRequirementsRepository->countDistinct('humidity_level');

      return Inertia::render('SinglePlant', compact('user', 'plant', 'is_demo', 'difficulty_max', 'exposition_max', 'humidity_max'));
    }
}
