<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Zone;
use App\Models\Repositories\ZoneRepository;
use App\Models\Repositories\PlantRepository;
use App\Models\Repositories\TaskRepository;
use App\Models\Repositories\SayingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Collection;


class DashBoardController extends Controller
{
    private $zoneRepository;
    private $plantRepository;
    private $taskRepository;
    private $sayingRepository;

    public function __construct(ZoneRepository $zoneRepository, PlantRepository $plantRepository,TaskRepository $taskRepository, SayingRepository $sayingRepository ) {
        $this->zoneRepository = $zoneRepository;
        $this->plantRepository = $plantRepository;
        $this->taskRepository = $taskRepository;
        $this->sayingRepository = $sayingRepository;
    }
    public function index()
    {
        $user = User::findOrFail(Auth::id());
        $current_garden = $user->currentTeam()->first();
        $zones = $this->zoneRepository->getByGarden($current_garden);
        $is_demo = $user->hasRole(User::ROLE_DEMO); # FIXME : This returns bool false everytime. 
        $saying = $this->sayingRepository->getByDate(date("Y-m-d"));

        $plants = $this->plantRepository->getByGarden($current_garden);
        $tasks = $this->taskRepository->getByGarden($current_garden)->slice(0, 5); // Display only the 5 first

        return Inertia::render('Dashboard', compact('user', 'is_demo', 'zones', 'saying', 'tasks', 'plants'));
    }

    public function update(Request $request, $id)
    {
        $zone = Zone::findOrFail($id);
        $zone->update($request->all());
        $zone->save();
        return back();
    }

}