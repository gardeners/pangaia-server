<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Pangaia Documentation",
 *      description="API for plant management",
 *      @OA\Contact(
 *          email="erwann@duclos.xyz"
 *      ),
 *      @OA\License(
 *          name="Hippocratic License HL3-CORE",
 *          url="https://firstdonoharm.dev/version/3/0/core.html"
 *      )
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Pangaia API Server"
 * )

    *
    * @OA\Tag(
    *     name="Pangaia API",
    *     description="API Endpoints of Pangaia"
    * )
    */
class Controller extends BaseController
{

    use AuthorizesRequests, ValidatesRequests;
}
