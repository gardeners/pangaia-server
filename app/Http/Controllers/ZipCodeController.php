<?php

namespace App\Http\Controllers;

use App\Models\Weather;
use App\Models\ZipCode;
use Illuminate\Http\Request;

class ZipCodeController extends Controller
{

  public function getWeather($zipcode)
  {
    $city = ZipCode::where('code', $zipcode)->with('weather')->first();
    $weather = $city->weather()->first();
    $openweathermapcode = $weather->openweathermapcode()->first();
    $data = ["city" => $city->name, "temp" => round($weather->temp, 0), "description" => $openweathermapcode->description, "name" => $openweathermapcode->name, "weathercode" => $openweathermapcode->id];
    return response()->json($data);
  }

  public function store(Request $request)
  {
    $request->validate([
      'code' => 'required|numeric',
    ]);

    $zipCode = ZipCode::firstOrCreate([
      'code' => $request->input('code'),
    ]);

    return response()->json(['message' => 'Zip code created', 'zip_code' => $zipCode]);
  }
}