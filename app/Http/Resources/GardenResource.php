<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @Oa\Schema(
 *    schema="GardenResource",
 *    type="object",
 *    title="PictureResource",
 *    description="Picture definition resource",
 * 
 *    @OA\Property(
 *        property="id",
 *       type="integer"
 *    ),
 *    @OA\Property(
 *      property="user_id",
 *      type="integer"
 *    ),
 *    @OA\Property(
 *      property="name",
 *      type="string"
 *     ),
 *     @OA\Property(
 *       property="personal_team",
 *       type="boolean"
 *     ),
 *     @OA\Property(
 *       property="created_at",
 *       type="string",
 *       format="date-time"
 *      ),
 *      @OA\Property(
 *        property="updated_at",
 *        type="string",
 *        format="date-time"
 *      ),
 *      @OA\Property(
 *        property="zones",
 *        type="array",
 *        description="Zones associated with the Garden. This property is omitted if the Zone is accessed from a Garden.",
 *        @OA\Items(ref="#/components/schemas/ZoneResource")
 *      )
 *  )
 */
class GardenResource extends JsonResource
{
    // public static $wrap = null;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'name' => $this->name,
            'personal_team' => $this->personal_team,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'zones' => ZoneResource::collection($this->whenLoaded('zones'))->additional(['fromGarden' => true]),
        ];
    }
}