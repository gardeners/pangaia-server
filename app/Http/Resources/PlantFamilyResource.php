<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="PlantFamilyResource",
 *     description="Plant family resource",
 *     @OA\Xml(name="PlantFamilyResource"),
 * 
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="Unique identifier for the PlantFamily."
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="Name of the PlantFamily."
 *     )
 * )
 */
class PlantFamilyResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}