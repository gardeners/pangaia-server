<?php

namespace App\Http\Resources;

use App\Models\PlantDefinition;
use Illuminate\Http\Resources\Json\JsonResource;


class InventoryPlantDefinitionResource extends JsonResource
{
  // Remove the wrapping data from the response
  public static $wrap = null;

  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    $pictureResource = new PictureResource($this->pictures->where('is_main', 1)->first());
    $picturePath = PlantDefinition::PLANT_DEF_PICTURES_PATH . $pictureResource->picture_name;

    return [
      'id' => $this->id,
      'name' => $this->name,
      'mainPicture' => $picturePath,
      'quantity' => $this->pivot->quantity,
      'updated_at' => $this->pivot->updated_at,
      'created_at' => $this->pivot->created_at,
    ];
  }
}
