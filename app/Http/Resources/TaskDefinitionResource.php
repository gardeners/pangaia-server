<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *   schema="TaskDefinitionResource",
 *   type="object",
 *   title="TaskDefinitionResource",
 *   description="Representation of a Task Definition.",
 *   @OA\Xml(name="TaskDefinitionResource"),
 * 
 *   @OA\Property(
 *     property="id",
 *     type="integer",
 *     description="Unique identifier of the Task Definition."
 *   ),
 *   @OA\Property(
 *     property="name",
 *     type="string",
 *     description="Name of the Task Definition."
 *   ),
 *   @OA\Property(
 *     property="description",
 *     type="string",
 *     description="Description of the Task Definition."
 *   ),
 *   @OA\Property(
 *     property="category",
 *     type="string",
 *     description="Category of the Task Definition."
 *   ),
 *   @OA\Property(
 *     property="date_beg",
 *     type="string",
 *     format="date",
 *     description="Beginning date of the Task Definition's validity."
 *   ),
 *   @OA\Property(
 *     property="date_end",
 *     type="string",
 *     format="date",
 *     description="Ending date of the Task Definition's validity."
 *   ),
 *   @OA\Property(
 *     property="task_definition_type",
 *     type="object",
 *     description="The type of the Task Definition.",
 *     @OA\Schema(ref="#/components/schemas/TaskDefinitionTypeResource")
 *   ),
 *   @OA\Property(
 *     property="plant_definition",
 *     type="object",
 *     description="The plant definition associated with the Task Definition.",
 *     @OA\Schema(ref="#/components/schemas/PlantDefinitionResource")
 *   )
 * )
 */
class TaskDefinitionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => $this->category,
            'date_beg' => $this->date_beg,
            'date_end' => $this->date_end,
            'task_definition_type' => new TaskDefinitionTypeResource($this->whenNotNull($this->task_definition_type)),
            'plant_definition' => new PlantDefinitionResource($this->whenNotNull($this->plant_definition)),
        ];
    }
}
