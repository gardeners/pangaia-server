<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="ZoneResource",
 *     description="Zone resource",
 *     @OA\Xml(name="ZoneResource"),
 * 
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="Unique identifier for the Zone."
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="Name of the Zone."
 *     ),
 *     @OA\Property(
 *         property="width",
 *         type="number",
 *         format="float",
 *         description="Width of the Zone."
 *     ),
 *     @OA\Property(
 *         property="height",
 *         type="number",
 *         format="float",
 *         description="Height of the Zone."
 *     ),
 *     @OA\Property(
 *         property="position_top",
 *         type="number",
 *         format="float",
 *         description="Top position of the Zone in its container."
 *     ),
 *     @OA\Property(
 *         property="position_left",
 *         type="number",
 *         format="float",
 *         description="Left position of the Zone in its container."
 *     ),
 *     @OA\Property(
 *         property="garden",
 *         ref="#/components/schemas/GardenResource",
 *         description="Garden to which the Zone belongs. Omitted if accessed from a GardenResource."
 *     ),
 *     @OA\Property(
 *         property="plants",
 *         type="array",
 *         @OA\Items(ref="#/components/schemas/PlantResource"),
 *         description="Collection of Plants within the Zone."
 *     ),
 *     @OA\Property(
 *         property="tasks",
 *         type="array",
 *         @OA\Items(ref="#/components/schemas/TaskResource"),
 *         description="Collection of Tasks associated with the Zone."
 *     )
 * )
 */
class ZoneResource extends JsonResource
{
  /**
   * Transform the resource collection into an array.
   * 
   * @param Request $request
   * @return array
   */
  public function toArray($request): array
  {
    // Check if the resource is being accessed from a GardenResource
    $fromGarden = $request->input('fromGarden', false);

    $data = [
      'id' => $this->id,
      'name' => $this->name,
      'width' => $this->width,
      'height' => $this->height,
      'position_top' => $this->position_top,
      "position_left" => $this->position_left,
      'garden' => $fromGarden ? null : new GardenResource($this->whenNotNull($this->garden)),
      'plants' => PlantResource::collection($this->whenLoaded('plants'))->additional(['fromZone' => true]),
      'tasks' => TaskResource::collection($this->whenLoaded('tasks')),
    ];

    // Remove the garden key entirely if accessed from GardenResource
    if ($fromGarden) {
      unset($data['garden']);
    }

    return $data;
  }
}
