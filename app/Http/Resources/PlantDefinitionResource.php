<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="PlantDefinitionResource",
 *     description="Plant definition resource",
 *     @OA\Xml(name="PlantDefinitionResource"),
 * 
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="Unique identifier for the PlantDefinition."
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="Name of the PlantDefinition."
 *     ),
 *     @OA\Property(
 *         property="family",
 *         type="object",
 *         description="Family of the PlantDefinition.",
 *         ref="#/components/schemas/PlantFamilyResource"
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string",
 *         description="Description of the PlantDefinition."
 *     ),
 *     @OA\Property(
 *         property="pictures",
 *         type="array",
 *         description="Pictures of the PlantDefinition.",
 *         @OA\Items(ref="#/components/schemas/PictureResource")
 *     )   
 * )
 */
class PlantDefinitionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<int|string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'family' => new PlantFamilyResource($this->whenLoaded('plantFamily')),
            'description' => $this->description,
            'pictures' => PictureResource::collection($this->whenLoaded('pictures')),
        ];
    }
}
