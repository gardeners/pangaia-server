<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 *  @OA\Schema(
 *    schema="GardenCollection",
 *    type="object",
 *  @OA\Property(
 *     property="id",
 *     type="integer"
 *  ),
 *  @OA\Property(
 *    property="user_id",
 *    type="integer"
 *  ),
 *  @OA\Property(
 *    property="name",
 *    type="string"
 *  ),
 *  @OA\Property(
 *    property="personal_team",
 *    type="boolean"
 *   ),
 *   @OA\Property(
 *     property="created_at",
 *     type="string",
 *     format="date-time"
 *   ),
 *   @OA\Property(
 *     property="updated_at",
 *     type="string",
 *     format="date-time"
 *    ),
 * )
 */
class GardenCollection extends ResourceCollection
{
  // public static $wrap = null;

  /**
   * Transform the resource collection into an array.
   * @param  \Illuminate\Http\Request  $request
   * @return array<int|string, mixed>
   */
  public function toArray(Request $request): array
  {
    return parent::toArray($request);
  }
}
