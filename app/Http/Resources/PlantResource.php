<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     title="PlantResource",
 *     description="Plant resource",
 *     @OA\Xml(name="PlantResource"),
 * 
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="Unique identifier for the Plant."
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="Name of the Plant."
 *     ),
 *     @OA\Property(
 *         property="cultivar",
 *         type="string",
 *         description="Cultivar of the Plant.",
 *         nullable=true
 *     ),
 *     @OA\Property(
 *         property="description",
 *         type="string",
 *         description="Description of the Plant.",
 *         nullable=true
 *     ),
 *     @OA\Property(
 *         property="plant_definition",
 *         type="object",
 *         description="Definition of the Plant.",
 *         ref="#/components/schemas/PlantDefinitionResource"
 *     ),
 *     @OA\Property(
 *         property="zone",
 *         type="object",
 *         description="Zone where the Plant is located. This property is omitted if the Plant is accessed from a Zone.",
 *         nullable=true,
 *         ref="#/components/schemas/ZoneResource"
 *     ),
 *     @OA\Property(
 *         property="tasks",
 *         type="array",
 *         description="Tasks associated with the Plant.",
 *         @OA\Items(ref="#/components/schemas/TaskResource")
 *     )
 * )
 */
class PlantResource extends JsonResource
{
  
  /**
   * Transform the resource into an array.
   *
   * @param Request $request
   * @return array
   */
  public function toArray(Request $request): array
  {
    // Check if the resource is being accessed from a GardenResource
    $fromZone = $request->input('fromZone', false);
    
    $data = [
      'id' => $this->id,
      'name' => $this->name,
      'cultivar' =>  $this->whenNotNull($this->cultivar),
      'description' =>  $this->whenNotNull($this->description),
      'plant_definition' => new PlantDefinitionResource($this->plant_definition),
      'zone' => new ZoneResource($this->whenNotNull($this->zone)),
      'tasks' => TaskResource::collection($this->whenLoaded('tasks')),
    ];

    if($fromZone) {
      unset($data['zone']);
    }

    return $data;
  }
}
