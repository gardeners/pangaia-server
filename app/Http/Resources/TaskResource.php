<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   schema="TaskResource",
 *   type="object",
 *   title="TaskResource",
 *   description="TaskResource representation.",
 *   @OA\Xml(name="TaskResource"),
 * 
 *   @OA\Property(
 *     property="id",
 *     type="integer",
 *     description="Unique identifier for the Task."
 *   ),
 *   @OA\Property(
 *     property="name",
 *     type="string",
 *     description="Name of the Task."
 *   ),
 *   @OA\Property(
 *     property="task_definition",
 *     type="object",
 *     description="Definition of the Task.",
 *     ref="#/components/schemas/TaskDefinitionResource"
 *   ),
 *   @OA\Property(
 *     property="description",
 *     type="string",
 *     description="Description of the Task."
 *   ),
 *   @OA\Property(
 *     property="date_beg",
 *     type="string",
 *     format="date",
 *     description="Beginning date of the Task."
 *   ),
 *   @OA\Property(
 *     property="date_end",
 *     type="string",
 *     format="date",
 *     description="Ending date of the Task."
 *   ),
 *   @OA\Property(
 *     property="zone",
 *     type="object",
 *     description="Zone where the Task is located.",
 *     ref="#/components/schemas/ZoneResource"
 *   ),
 *   @OA\Property(
 *     property="plant",
 *     type="object",
 *     description="Plant associated with the Task.",
 *     ref="#/components/schemas/PlantResource"
 *   )
 * )
 */
class TaskResource extends JsonResource
{
  /**
   * Transform the resource collection into an array.
   *
   * @return array<int|string, mixed>
   */
  public function toArray(Request $request): array
  {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'task_definition' => new TaskDefinitionResource($this->whenNotNull($this->task_definition)),
      'description' => $this->description,
      'date_beg' => $this->date_beg,
      'date_end' => $this->date_end,
      'zone' => new ZoneResource($this->whenNotNull($this->zone)),
      'plant' => new PlantResource($this->whenNotNull($this->plant)),
    ];
  }
}
