<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'task_definition_id' => 'required',
            'zone_id' => 'required_if:plant_id,null',
            'plant_id' => 'required_if:zone_id,null',
            'date_beg' => 'date',
            'date_end' => 'nullable|date|after_or_equal:date_beg'
        ];
        return $rules;

    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'zone_id.required' => "Either a zone or a plant should be associated"
        ];
    }
}
