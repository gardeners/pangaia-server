<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\SayingRequest;

class UpdateSayingRequest extends SayingRequest
{
    use ApiRequestTrait;
}
