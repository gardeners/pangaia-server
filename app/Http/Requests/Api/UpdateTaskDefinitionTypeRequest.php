<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskDefinitionTypeRequest;

class UpdateTaskDefinitionTypeRequest extends TaskDefinitionTypeRequest
{
    use ApiRequestTrait;
}
