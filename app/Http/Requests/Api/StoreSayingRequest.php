<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\SayingRequest;
class StoreSayingRequest extends SayingRequest
{
    use ApiRequestTrait;
}
