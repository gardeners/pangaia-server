<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskDefinitionTypeRequest;

class StoreTaskDefinitionTypeRequest extends TaskDefinitionTypeRequest
{
    use ApiRequestTrait;
}
