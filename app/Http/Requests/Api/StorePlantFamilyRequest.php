<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\PlantFamilyRequest;

class StorePlantFamilyRequest extends PlantFamilyRequest
{
    use ApiRequestTrait;
}
