<?php

namespace App\Traits;

use Laravel\Jetstream\HasProfilePhoto as BaseHasProfilePhoto;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

trait CustomHasProfilePhoto
{
    use BaseHasProfilePhoto;

    public function profilePhotoUrl()
    {
        return $this->profile_photo_path
            ? str_replace('//', '/', Storage::disk($this->profilePhotoDisk())->url($this->profile_photo_path))
            : $this->defaultProfilePhotoUrl();
    }

    /**
     * Get the default profile photo URL if no profile photo has been uploaded.
     *
     * @return string
     */
    protected function defaultProfilePhotoUrl()
    {
        return 'https://ui-avatars.com/api/?name='.urlencode($this->name).'&color=ffffff&background=038340';
    }
}