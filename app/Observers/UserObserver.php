<?php

namespace App\Observers;

use App\Models\Hs\HsProfile;
use App\Models\User;

/**
 * Observer on User. 
 * https://laravel.com/docs/8.x/eloquent#observers
 */
class UserObserver
{
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        // On user creation a default hs profile is generated
        HsProfile::factory()->create([
            "name" => $user->name,
            "user_id" => $user->id
        ]);
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        //
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}