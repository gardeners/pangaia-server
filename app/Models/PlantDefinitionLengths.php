<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinitionLengths extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'plant_definition_lengths';
    
    protected $fillable = [
        'min_height',
        'max_height',
    ];

    public static function countDistinct($column)
    {
        return self::distinct()->count($column);
    }

    public function plant_definition()
    {
        return $this->hasMany(PlantDefinition::class, 'lenght_id', 'id');
    }
}
