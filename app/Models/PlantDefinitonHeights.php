<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Services\ConversionService;

class PlantDefinitonHeights extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $conversionService;
    
    protected $fillable = [
        'min_height',
        'max_height',
    ];

    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->conversionService = app(ConversionService::class);
    }

    public function getMinHeightAttribute($value)
    {
        return $this->conversionService->cmToM($value);
    }

    public function getMaxHeightAttribute($value)
    {
        return $this->conversionService->cmToM($value);
    }
}
