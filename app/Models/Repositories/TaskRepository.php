<?php
namespace App\Models\Repositories;

use App\Models\Plant;
use App\Models\Task;
use App\Models\Team;

class TaskRepository extends BaseRepository
{
    public function __construct(Task $model)
    {
        parent::__construct($model);
    }

    public function getByPlant(Plant $plant)
    {
        return $this->model->where('plant_id', $plant->id)->get();
    }

    /***
     * List all tasks (plant tasks and zone tasks) 
     * Ordered per date_beg
     * Older than today
     **/
    public function getByGarden(Team $garden)
    {
        $plant_tasks = $this->model
            ->select('tasks.*')
            ->join('plants', 'plant_id', '=', 'plants.id')
            ->join('zones', 'plants.zone_id', '=', 'zones.id')
            ->where('zones.garden_id', $garden->id);

        $zone_tasks = $this->model
            ->select('tasks.*')
            ->join('zones', 'zone_id', '=', 'zones.id')
            ->where('zones.garden_id', $garden->id);

        return $plant_tasks->union($zone_tasks)->whereDate('date_beg', '>=', date('Y-m-d'))->orderBy('date_beg')->get();
    }
}