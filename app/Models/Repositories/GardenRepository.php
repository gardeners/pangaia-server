<?php

namespace App\Models\Repositories;

use App\Models\Garden;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;


class GardenRepository extends BaseRepository
{

  public function __construct(Garden $model)
  {
    parent::__construct($model);
  }

  /**
   * Get all the gardens owned by the user and the gardens the user is a member of.
   *
   * @param int $user_id
   * @return \Illuminate\Database\Eloquent\Collection 
   */
  public function getAllGardensByUser($user_id)
  {
    return Garden::AllOfUser($user_id)->get();
  }

  /**
   * Get the garden of the user.
   *
   * @param int $user_id
   * @param int $garden_id
   * @return Garden
   */
  public function getSingleGardenByUser($user_id, $garden_id)
  {
    return Garden::SingleOfUser($user_id, $garden_id)->firstOrFail();
  }

  /**
   * Create a garden for the user.
   * 
   * @param int $user_id
   * @param array $data
   * @return Garden
   */
  public function createGarden(int $user_id, $data)
  {
    /** @var Garden $garden */
    $user = User::findOrFail($user_id);
    $garden = $user->ownedTeams()->firstOrCreate([
      'user_id' => $user_id,
      'name' => $data['name'],
      'personal_team' => $data['personal_team'] ?? true,
    ]);
    return $garden;
  }

  public function updateGarden($user_id, $garden_id, $data)
  {
    $garden = Garden::SingleOfUser($user_id, $garden_id)->firstOrFail();
    $garden->update($data);
    return $garden;
  }

  public function deleteGarden($user_id, $garden_id)
  {
    $garden = Garden::SingleOfUser($user_id, $garden_id)->firstOrFail();
    $garden->delete();
    return $garden;
  }
}
