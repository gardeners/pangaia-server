<?php

namespace App\Models\Repositories;

use App\Models\PlantDefinitionRequirements;
use App\Models\Repositories\BaseRepository;

class PlantDefinitionRequirementsRepository extends BaseRepository
{
  public function __construct(PlantDefinitionRequirements $model)
  {
    parent::__construct($model);
  }

  public function countDistinct($column)
  {
    return $this->model->distinct()->count($column);
  }
}