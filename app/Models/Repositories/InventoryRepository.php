<?php

namespace App\Models\Repositories;

use App\Http\Resources\InventoryResource;
use App\Models\PlantDefinition;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class InventoryRepository extends BaseRepository
{
  protected $model;
  protected $plantDefinitionRepository;

  public function __construct(User $model, PlantDefinitionRepository $plantDefinitionRepository)
  {
    parent::__construct($model);
    $this->plantDefinitionRepository = $plantDefinitionRepository;
  }

  /**
   * Get user by id
   * 
   * @param int $id
   * @return User|null
   */
  public function find($id)
  {
    return $this->model->find($id);
  }

  /**
   * Get the inventory of the user select all data from plant definitions attach the main picture of each plant.
   * Plant definitions data is sorted in InventoryPlantDefinitionResource.
   * Sort the data in InventoryResource.
   * 
   * @param int $id
   * @return User|null
   */
  public function inventory($id)
  {
    $user = $this->model
      ->with([
        'plantDefinitions' => function ($query) {
          $query->with([
            'pictures' => function ($query) {
              $query->where('is_main', 1);
            },
          ]);
        },
      ])
      ->find($id);

    return new InventoryResource($user);
  }


  /**
   * Find a plant in the inventory of the user
   * 
   * @param int $user_id
   * @param int $plant_definition_id
   * @return PlantDefinition|null
   */
  public function findPlant($user_id, $plant_definition_id)
  {
    $user = $this->find($user_id);
    $plant = $user->plantDefinitions()->where('id', $plant_definition_id)->firstOrFail();

    return $plant;
  }

  /**
   * Update the inventory of the user
   * 
   * @param int $user_id
   * @param int $plant_definition_id
   * @param int $quantity
   * @return User
   */
  public function postInventory($user_id, $plant_definition_id, $quantity)
  {
    // Find the user
    $user = $this->find($user_id);
  
    try {
      PlantDefinition::findOrFail($plant_definition_id);
    } catch (ModelNotFoundException $e) {
      throw new ModelNotFoundException("PlantDefinition could not be found.");
    }
  
    // Check if the Plant is already in the User's Inventory
    $plantInInventory = $user->plantDefinitions()->where('id', $plant_definition_id)->exists();
    if ($plantInInventory) {
      abort(422, "Plant is already in the inventory.");
    }
  
    // If the PlantDefinition exists and the Plant is not in the inventory, attach it
    $user->plantDefinitions()->syncWithoutDetaching([
      $plant_definition_id => ['quantity' => $quantity]
    ]);
  
    return $user->plantDefinitions()->where('id', $plant_definition_id)->first();
  }

  /**
   * Update the inventory of the user
   * 
   * @param int $user_id
   * @param int $plant_definition_id
   * @param int $quantity
   * @return User
   */
  public function updateInventory($user_id, $plant_definition_id, $quantity)
  {
    try {    
      $plant = $this->findPlant($user_id, $plant_definition_id);

      // Update its quantity
      $user = $this->find($user_id);
      $user->plantDefinitions()->updateExistingPivot($plant_definition_id, ['quantity' => $quantity]);
      
      return $plant;
    } catch (ModelNotFoundException $e) {
      throw new ModelNotFoundException("Plant not found in the inventory.");
    }
  }

  /**
   * Remove a plant definition from the user's inventory
   * 
   * @param int $user_id
   * @param int $plant_definition_id
   * @return User
   */
  public function removePlantDefinition($user_id, $plant_definition_id)
  {
    $user = $this->find($user_id);
  
    // Check if the plant is associated with the user
    $plant = $user->plantDefinitions()->find($plant_definition_id);
  
    if (!$plant) {
      throw new ModelNotFoundException("Plant not found in the inventory.");
    }
  
    // If the plant exists, detach it from the user's inventory
    $user->plantDefinitions()->detach($plant_definition_id);
  
    return response()->json(['message' => "Plant with id: {$plant_definition_id} removed from the inventory"], 200);
  }
} 
