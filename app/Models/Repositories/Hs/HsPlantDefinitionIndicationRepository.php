<?php
namespace App\Models\Repositories\Hs;

use App\Models\Hs\HsPlantDefinitionIndication;
use App\Models\Repositories\BaseRepository;

class HsPlantDefinitionIndicationRepository extends BaseRepository
{
    public function __construct(HsPlantDefinitionIndication $model)
    {
        parent::__construct($model);
    }


    public function getByHsPlantDefinitionId($plantDefinitionId)
    {
        return $this->model
            ->where('hs_plant_definition_id', $plantDefinitionId)
            ->get();
    }
}
