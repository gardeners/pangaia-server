<?php
namespace App\Models\Repositories\Hs;

use App\Models\Repositories\BaseRepository;
use App\Models\Hs\HsLevel;

class HsLevelRepository extends BaseRepository
{
    public function __construct(HsLevel $model)
    {
        parent::__construct($model);
    }


    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }
}
