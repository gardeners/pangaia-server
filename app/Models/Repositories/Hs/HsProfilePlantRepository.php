<?php
namespace App\Models\Repositories\Hs;

use App\Models\Hs\HsProfilePlant;
use App\Models\Repositories\BaseRepository;

class HsProfilePlantRepository extends BaseRepository
{
    public function __construct(HsProfilePlant $model)
    {
        parent::__construct($model);
    }


    public function  getByHsProfileId($hsProfileId)
    {
        return $this->model
            ->where('hs_profile_id', $hsProfileId)
            ->get();
    }

    public function getByHsPlantDefinitionId($hsPlantDefinitionId)
    {
        return $this->model
            ->where('hs_plant_definitions_id', $hsPlantDefinitionId)
            ->get();
    }

}
