<?php
namespace App\Models\Repositories;

use App\Models\Plant;
use App\Models\Zone;
use App\Models\Team;

class PlantRepository extends BaseRepository
{
    public function __construct(Plant $model)
    {
        parent::__construct($model);
    }

    /**
     * Get all plants by zone
     * 
     * @param Zone $zone
     * @return mixed
     */
    public function getByZone(Zone $zone)
    {
        return $this->model->where('zone_id', $zone->id)->get();
    }

    /**
     * Get all plants by garden
     *
     * @param Team $garden
     * @return mixed
     */
    public function getByGarden(Team $garden)
    {
        $models = $this->model
            ->join('zones', 'zone_id', '=', 'zones.id')
            ->where('zones.garden_id', $garden->id)
            ->get();
        return $models;
    }

}