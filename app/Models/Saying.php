<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Saying extends Model
{
  use CrudTrait;
  use HasFactory;

  protected $fillable = [
    'title',
    'date_beg',
    'date_end'
  ];
}
