<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantFamily extends Model
{
    use CrudTrait;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Get the plantDefinitions for this model.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function plantDefinitions()
    {
        return $this->hasMany(PlantDefinition::class, 'family_id', 'id');
    }
}
