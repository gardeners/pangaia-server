<?php

namespace App\Models\Hs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class HsPlantDefinitionIndication extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'message',
        'step',
    ];

    public function hs_plant_definition()
    {
        return $this->belongsTo('App\Models\Hs\HsPlantDefinition', 'hs_plant_definition_id');
    }
}
