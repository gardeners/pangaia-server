<?php

namespace App\Models\Hs;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class HsPlantDefinition extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'level',
        'plant_definitions_id'
    ];

    public function plant_definition()
    {
        return $this->belongsTo('App\Models\PlantDefinition', 'plant_definitions_id');
    }
    public function hs_plant_definition_indication()
    {
        return $this->hasMany(HsPlantDefinitionIndication::class);
    }
}
