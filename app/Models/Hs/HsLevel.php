<?php

namespace App\Models\Hs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
class HsLevel extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'min_experience',
    ];
}
