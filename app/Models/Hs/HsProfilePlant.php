<?php

namespace App\Models\Hs;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HsProfilePlant extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'hs_profile_id',
        'hs_plant_definitions_id',
        'found_date',
    ];

    public function hs_plant_definition()
    {
        return $this->belongsTo('App\Models\Hs\HsPlantDefinition', 'hs_plant_definitions_id');
    }

    public function hs_profile()
    {
        return $this->belongsTo('App\Models\Hs\HsProfile', 'hs_profile_id');
    }
}
