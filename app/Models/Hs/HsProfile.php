<?php

namespace App\Models\Hs;

use App\Models\Traits\ExperienceTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HsProfile extends Model
{
    use CrudTrait;
    use HasFactory;
    use ExperienceTrait;




    protected $fillable = [
        'name',
        'user_id',
        'experience',
        'profile_picture',
        'level'
    ];

    protected $attributes = [
        'experience' => 0,
        'level' => 1
    ];

    public function hs_level()
    {
        return $this->belongsTo('App\Models\Hs\HsLevel', 'level');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
