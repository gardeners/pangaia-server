<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinition extends Model
{
  use CrudTrait;
  use HasFactory;

  const PLANT_DEF_PICTURES_PATH = '/GraphicRessources/Images/Plants/';

  /**
   * Hide the columns that are not needed in the frontend
   */
  protected $hidden = [
    'height_id',
    'requirement_id',
    'difficulty_id',
    'wideness_id',
    'family_id',
  ];

  /**
   * Attributes that should be mass-assignable.
   *
   * @var array
   */
  protected $fillable = [
    'family',
    'description',
    'latin_name',
    'difficulty_id',
    'requirement_id',
    'height_id',
    'wideness_id',
    'family_id',
  ];

  /**
   * Attributes that have default values.
   */
  protected $attributes = [
    'description' => 'toBeDefined',
    'latin_name' => 'toBeDefined',
  ];

  protected $appends = [
    'name', 
    'picture'
  ];

  /**
   * Get the most common alternate plant name associated with the plant definition.
   */
  public function getNameAttribute()
  {
    $names = AlternatePlantName::whereHas('plantDefinitions', function ($q) {
        $q->where([['plant_definition_id', '=', $this->id], ['is_most_common_plant_name', '=', true]]);
    })->first();
    return optional($names)->name;
  }

  /**
   * Get the most common picture associated with the plant definition.
   */
  public function getPictureAttribute()
  {
      $picture = Pictures::where([['imageable_id', '=', $this->id], ['is_main', '=', true]])->first();
      return $picture ? self::PLANT_DEF_PICTURES_PATH . $picture->picture_name : null;
  }

  /**
   * Get the alternate plant names associated with the plant definition.
   */
  public function alternatePlantNames()
  {
    return $this->belongsToMany(AlternatePlantName::class, 'plant_def_has_alt_name');
  }

  /**
   * Get the taskDefinition for PlantDefiniton.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function taskDefinitions()
  {
    return $this->hasMany('App\Models\TaskDefinition', 'plant_definition_id', 'id');
  }

  /**
   * Get the plantDefinitionIndications for PlantDefiniton.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function plant_definition_indications()
  {
    return $this->hasMany('App\Models\Hs\PlantDefinitionIndication', 'plant_definition_id', 'id');
  }

  /**
   * Get the plantDefinitionDifficulty for this model.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function difficulty()
  {
    return $this->belongsTo(Difficulty::class, 'difficulty_id', 'id');
  }

  /**
   * Get the plantDefinitionRequirements for this model.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function requirements()
  {
    return $this->belongsTo(PlantDefinitionRequirements::class, 'requirement_id', 'id');
  }

  /**
   * Get the plantDefinitionHeight for this model.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function heights()
  {
    return $this->belongsTo(PlantDefinitionHeights::class, 'height_id', 'id');
  }

  /**
   * Get the plantDefinitionWidness for this model.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function widenesses()
  {
    return $this->belongsTo(PlantDefinitionWidenesses::class, 'wideness_id', 'id');
  }

  /**
   * Get all of pictures for the PlantDefinition
   */
  public function pictures()
  {
    return $this->morphMany(Pictures::class, 'imageable');
  }

  /**
   * Get the relation between the plantDefinition and the family
   */
  public function plantFamily()
  {
    return $this->belongsTo(PlantFamily::class, 'family_id', 'id');
  }

  /**
   * Get the plantDefinitionDifficulty for this model.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function inventory()
  {
    return $this->belongsToMany(User::class, 'users_inventory', 'plant_definition_id', 'user_id')
      ->withPivot('quantity')
      ->withTimestamps();
  }
}
