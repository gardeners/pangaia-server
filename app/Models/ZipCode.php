<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
  protected $fillable = [
    'code',
  ];

  /**
   * Get the weather for this model.
   */
  public function weather()
  {
    return $this->hasOne('App\Models\Weather')->latest();
  }
}