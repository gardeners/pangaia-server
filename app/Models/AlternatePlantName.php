<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlternatePlantName extends Model
{
    use CrudTrait;
    use HasFactory;

    public $timestamps = true;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Attributes that have default values.
     */
    protected $attributes = [
        'name' => 'defaultName',
    ];

    /**
     * Get the plant definitions associated with the alternate plant name.
     */
    public function plantDefinitions()
    {
        return $this->belongsToMany(PlantDefinition::class, 'plant_def_has_alt_name');
    }

}