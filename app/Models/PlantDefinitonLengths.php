<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinitonLengths extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'plant_definition_lengths';
    
    protected $fillable = [
        'min_height',
        'max_height',
    ];
}
