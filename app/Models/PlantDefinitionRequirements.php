<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlantDefinitionRequirements extends Model
{
    use CrudTrait;
    use HasFactory;


    protected $fillable = [
        'exposition_level',
        'exposition_label',
        'humidity_level',
        'humidity_label',
    ];
    
    public function plantDefinition()
    {
        return $this->hasMany(PlantDefinition::class, 'requirement_id', 'id');
    }
}
