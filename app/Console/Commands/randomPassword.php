<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\PasswordGeneratorService;

class randomPassword extends Command
{
    protected $signature = 'generate:password';

    protected $description = 'Generates a random password';

    protected $passwordGenerator;

    public function __construct(PasswordGeneratorService $passwordGenerator)
    {
        parent::__construct();

        $this->passwordGenerator = $passwordGenerator;
    }

    public function handle()
    {
        $password = $this->passwordGenerator->generateRandomPassword();

        $this->info('Generated password: ' . $password . "\n");
    }
}