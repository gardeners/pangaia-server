<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DeleteOldDemoUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:delete-old-demo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old demo users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $affectedRows = User::where('email', 'like', '%demo@pangaia.app')
            ->where('created_at', '<', Carbon::now()->subDay())
            ->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->info($affectedRows . ' demo users deleted.');

        return 0;
    }
}