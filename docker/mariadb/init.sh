# Creating database for matomo service

mariadb -uroot -p"$MARIADB_ROOT_PASSWORD" --execute \
"CREATE DATABASE IF NOT EXISTS matomo;
GRANT ALL PRIVILEGES ON matomo.* TO '$MARIADB_USER'@'%' IDENTIFIED BY '$MARIADB_PASSWORD';
FLUSH PRIVILEGES;"