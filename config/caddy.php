<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authorized Domains
    |--------------------------------------------------------------------------
    |
    | Domains that are authorized to be viewed through Caddy.
    |
    */

    'authorized' => [
        'pangaia.app',
        'localhost',
    ],

];