import { defineStore } from "pinia";
import axios from "axios";

export const useInventoryStore = defineStore("useInventoryStore", {
  state: () => ({
    user_inventory: {},
  }),
  actions: {
    async fetchPlants(user_id) {
      const response = await axios.get(`/api/v1/user/${user_id}/inventory`);
      if (response.data) {
        // Sort inventory by updated_at in descending order
        response.data.inventory.sort((a, b) => new Date(b.updated_at) - new Date(a.updated_at));
        
        this.user_inventory = response.data;
        // Initialize count for each plant
        this.user_inventory.inventory.forEach(plant => {
          plant.count = this.isPlantInInventory(plant.id) ? plant.quantity : 0;
        });
      } else {
        console.error('Error: server response is not an object:', response.data);
      }
    },
    
    async addPlant(plant_id, user_id) {
      const response = await axios.post(`/api/v1/user/${user_id}/inventory`, {
        plant_definition_id: plant_id,
        quantity: 1
      });
      if (response.data && response.data.id) {
        // Find the added plant in the local state
        const plant = this.user_inventory.inventory.find(p => p.id === plant_id);
        if (plant) {
          // If the plant is already in the inventory, increment its count
          plant.count += 1;
        } else {
          // If the plant is not in the inventory, add it with a count of 1
          this.user_inventory.inventory.push({
            id: plant_id,
            name: response.data.name,
            mainPicture: response.data.mainPicture,
            count: 1
          });
        }
      } else {
        console.error('Unexpected response data:', response.data);
      }
    },

    async incrementCount(plant_id, user_id) {
      const plant = this.user_inventory.inventory.find(plant => plant.id === plant_id);
      if (plant) {
        plant.count++;
        await this.updatePlant(plant_id, plant.count, user_id);
      }
    },
    
    async decrementCount(plant_id, user_id) {
      const plant = this.user_inventory.inventory.find(plant => plant.id === plant_id);
      if (plant && plant.count > 0) {
        plant.count--;
        if (plant.count === 0) {
          await this.deletePlant(plant_id, user_id);
        } else {
          await this.updatePlant(plant_id, plant.count, user_id);
        }
      }
    },
    
    // #FIXME : Can easily be in 429 error Too many requests if the user click too fast. Should be triggered with a batch update
    async updatePlant(plant_id, count, user_id) {
      try {
        const response = await axios.put(`/api/v1/user/${user_id}/inventory/${plant_id}`, {
          quantity: count
        });
        if (response.data && response.data.inventory) {
          this.user_inventory = response.data;
          // Update count for the updated plant
          const plant = this.user_inventory.inventory.find(plant => plant.id === plant_id);
          if (plant) {
            plant.count = count;
          }
        } 
      } catch (error) {
        console.error(`Failed to update plant: ${error}`);
      }
    },

    async deletePlant(plant_id, user_id) {
      // Find the plant
      const plant = this.user_inventory.inventory.find(plant => plant.id === plant_id);
      
      // If the plant exists and the count is 0, delete it
      if (plant && plant.count === 0) {
        try {
          const response = await axios.delete(`/api/v1/user/${user_id}/inventory/${plant_id}`);
            // Remove the plant from the local state
            this.user_inventory.inventory = this.user_inventory.inventory.filter(plant => plant.id !== plant_id);
        } catch (error) {
          console.error(`Failed to delete plant: ${error}`);
        }
      }
    },

    isPlantInInventory(plantId) {
      const inventory = this.user_inventory.inventory;
      return inventory && inventory.some(plant => plant.id === plantId);
    },

    clearData() {
      this.user_inventory = {};
    },

  },
});