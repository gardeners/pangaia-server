import { defineStore } from 'pinia'
import {DEFAULT_ZIPCODE} from "../Models/Garden.js"
import axios from "axios";

export const useWeatherStore = defineStore('useWeatherStore', {
  state: () => ({
    temperature: 19,
    icon: '/GraphicRessources/Icons/weather/801.svg',
    name : 'Ensoleillé !!!',
    city: 'Rennes',
    country: 'France'
  }),
  actions: {
    async getWeatherForZipcode(zipcode = DEFAULT_ZIPCODE){
      try {
        const response = await axios.get(`/weather/${zipcode}`);
        this.temperature = response.data.temp
        this.city = response.data.city
        this.name = response.data.name
        this.description = response.data.description
        this.icon = `/GraphicRessources/Icons/weather/${response.data.weathercode}.svg`

      } catch (error) {
        console.error('Error reading zipcode from server:', error);
      }
    },
  },
})
