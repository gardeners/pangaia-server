import { defineStore } from 'pinia'

export const usePlantStore = defineStore('usePlantStore', { 
  state: () => ({
    plant : {},
  }),
  actions: {
    setPlant(plant) {
      this.plant = plant;
      this.convertPlantSizesToMeter(plant);

    },
    convertPlantSizesToMeter(plant) {
      const convertSize = (min, max) => {
        const convert = (size) => {
          if (size < 100) {
            return size + 'cm';
          } else {
            let meter = Math.floor(size / 100);
            let cm = size % 100 === 0 ? null : size % 100;
            return cm ? `${meter}m${cm}` : `${meter}m`;
          }
        };
      
        return min === max ? convert(min) : `${convert(min)} - ${convert(max)}`;
      };
    
      if (plant.widenesses) {
        plant.widenesses = {
          ...plant.widenesses,
          width: convertSize(plant.widenesses.min_width, plant.widenesses.max_width)
        };
      }
      
      if (plant.heights) {
        plant.heights = {
          ...plant.heights,
          height: convertSize(plant.heights.min_height, plant.heights.max_height)
        };
      }
    }
  },
})