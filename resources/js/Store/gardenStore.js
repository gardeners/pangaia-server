import { defineStore } from "pinia";
import axios from "axios";

export const useGardenStore = defineStore("useGardenStore", {
  state: () => ({
    userGardens: JSON.parse(localStorage.getItem("userGardens") || '{}'),
    storedZipcode: localStorage.getItem("zipcode") || null,
  }),
  actions: {
    initializeGarden(gardenData) {
      const userId = gardenData.user_id;
      let userGardens = localStorage.getItem("userGardens");

      if (this.storedZipcode) {
        gardenData.zipcode = parseInt(this.storedZipcode, 10);
        localStorage.removeItem("zipcode"); // this variable is only temporary during user creation

        const filteredGardenData = {
          id: gardenData.id,
          name: gardenData.name,
          zipcode: gardenData.zipcode,
        };
        userGardens = userGardens ? JSON.parse(userGardens) : {}

        if (!userGardens[userId]) {
          userGardens[userId] = {};
        }
        userGardens[userId][gardenData.id] = filteredGardenData;
        localStorage.setItem("userGardens", JSON.stringify(userGardens));
        // Update the store with the new garden
        this.userGardens = userGardens;
      }
    },
    saveZipcodeLocalStorage(zipcode) {
      const zipcodeInt = parseInt(zipcode, 10);
      if (!isNaN(zipcodeInt)) {
        this.storedZipcode = zipcodeInt;
        localStorage.setItem("zipcode", zipcodeInt);
        return true;
      } else {
        console.error("Invalid zipcode:", zipcode);
        return false;
      }
    },
    async saveZipcodeDatabase(zipcode) {
      const zipcodeInt = parseInt(zipcode, 10);
      if (!isNaN(zipcodeInt)) {
        try {
          await axios.post("/zipcode", { code: zipcodeInt});
          return true;
        } catch (error) {
          console.error("Error sending zipcode to server:", error);
          return false;
        }
      } else {
        console.error("Invalid zipcode:", zipcode);
        return false;
      }
    },
    deleteGardenFromLocalStorage(userId, gardenId) {
      if (this.userGardens[userId]) {
        delete this.userGardens[userId][gardenId];
        localStorage.setItem("userGardens", JSON.stringify(this.userGardens));
      }
    },
    deletZipcodeLocalStorage() {
      const zipcode = localStorage.getItem("zipcode")
      if (zipcode) {
        localStorage.removeItem("zipcode");
      } else {
        console.error("Error occured when removing zipcode: ", zipcode)
      }
    },
    getZipcode(userId, gardenId) {      
      if (this.userGardens[userId] && this.userGardens[userId][gardenId]) {
          return this.userGardens[userId][gardenId].zipcode;
      }
      return '';
    }
  },
});
