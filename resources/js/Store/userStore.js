import { defineStore } from 'pinia'
import { useRegisterStore } from './registerStore.js';

export const useUserStore = defineStore('useUserStore', { 
  state: () => ({
    user: {},
    isUserDemo: false,
    userZipcode: null,
    isBurgerActive: false,
  }),
  actions: {
    setUser(user) {
      // #FIXME - This is a temporary solution to show the user role if he is demo user - need to be done in the backend
      this.user = user;
      this.userName = user.name;
      this.isUserDemo = user.name.includes('Demo-');
    
      if (this.isUserDemo) {
        if (!this.user.roles) {
          this.user.roles = [{}];
        }
        if (!this.user.roles.some(role => role.name === 'Demo')) {
          this.user.roles.push({ name: 'Demo' });
        }
      }
    },
    getUserZipcode() {
      const registerStore = useRegisterStore();
      return this.userZipcode = registerStore.userZipcode;;
    },
    handleBurgerMenu() {
      this.isBurgerActive = !this.isBurgerActive;
      // console.log('isBurgerActive: ', this.isBurgerActive);
    },
    resetBurgerMenu() {
      // Avoid closing the menu directly when clicking on the menu items
      setTimeout(() => {
        this.isBurgerActive = false;
      }, 1000); 
    }
  },
})

