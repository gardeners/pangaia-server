import { defineStore } from 'pinia'

// You can name the return value of `defineStore()` anything you want,
// but it's best to use the name of the store and surround it with `use`
// and `Store` (e.g. `useUserStore`, `useCartStore`, `useProductStore`)
// the first argument is a unique id of the store across your application
export const useHsStore = defineStore('useHsStore', {
  state: () => ({ 
    selectedProfile: undefined,
    currentChallenge: undefined,
    pictureTaken: undefined
  }),
  getters: {
    experience: (state) => state.selectedProfile?.experience ?? 0,
  },
  actions: {
    chooseProfile(newProfile) {
      this.selectedProfile = {
        id : newProfile.id,
        name: newProfile.name
      }
    },
    takePicture(pictureTaken){
      this.pictureTaken = pictureTaken
    },
    setCurrentChallenge(plant) {
      this.currentChallenge = plant
    },
    clear() {
      this.pictureTaken = undefined

    },
    forgetCurrentChallenge() {
      this.currentChallenge = undefined

    },
    registerPlantFound() {
      // Save in back end that the plant has been found.
      axios.post(`/games/hs/plantFound`, {
        hs_profile_id: this.selectedProfile.id,
        hs_plant_definition_id: this.currentChallenge.value.id
      });
      // TODO : Check if this went successfully !  
    }
  },
})