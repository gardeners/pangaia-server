
export const levels = {
    noob: {
        level: 1,
        label: 'Jardinier en herbe',
        desc: {
            icon: '/GraphicRessources/Icons/JeunePousseIcon.svg',
            title: 'Jeune pousse',
            text: 'Les seuls légumes que j’ai vu de ma vie c’était au Monoprix.'
        },
    },
    rookie: {
        level: 2,
        label: 'Planteur fou',
        desc: {
            icon: "/GraphicRessources/Icons/CoupPousseIcon.svg",
            title: 'Coup de pousse',
            text: 'J’aime bien le jardinage mais j’ai la flemme de faire un Excel pour monitorer mon jardin.'
        },
    },
    gardener: {
        level: 3,
        label: 'Verte main',
        desc: {
            icon:  "/GraphicRessources/Icons/BigPousseIcon.svg",
            title: 'Big pousse',
            text: ' Le jardinage c’est ma passion, je connais toutes les plantes par leurs noms latins, au pictunari des plantes je suis imbattable.'
        },
    },
  }