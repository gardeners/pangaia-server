import { FormInterface } from './FormInterface.js';
import { generatePassword } from "../Commons/passwordGenerator";
import { roles }  from "./Role.js"
class DemoUser extends FormInterface {
    constructor(){
        super()
        this.name = this.generateRandomName();
        this.email = this.name + `@demo.pangaia.app`;
        this.password = "Pangaia c'est rigolo comme nom mouahahahaa !";
        
      
    }
    generateRandomName() {
        return "Demo-" + Math.random().toString(36).slice(2);
    }

     // Function to create a demo user form
    getForm() {
        return {
            name : this.name,
            email: this.email, 
            password: this.password, 
            password_confirmation: this.password,
            terms: true,
            canCreateTeams: true,
            roles: roles.demo
        }
    }

}
// Export the demo user form
export async function getDemoUserForm() {
    const demoUserForm = new DemoUser();
    await generatePassword(demoUserForm);
    demoUserForm.password_confirmation = demoUserForm.password
    return demoUserForm.getForm();
}