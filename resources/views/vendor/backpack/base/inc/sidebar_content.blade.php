{{-- This file is used to store sidebar items, inside the Backpack admin panel --}}
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<hr>
@can('zone.list')
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('zone') }}"><i class="nav-icon la la-draw-polygon"></i> Zones</a></li>
@endcan
@can('plant.list')
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('plant') }}"><i class="nav-icon la la-leaf"></i> Plants</a></li>
@endcan
@can('task.list')
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('task') }}"><i class="nav-icon la la-clipboard-list"></i> Tasks</a></li>
@endcan
@can('garden.list')
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('garden') }}"><i class="nav-icon la la-seedling"></i> Gardens</a></li>
@endcan

<hr>
@can('plant_definition.list')
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('plant-definition') }}"><i class="nav-icon la la-comment"></i> Plant definitions</a></li>
@endcan
@can('task_definition.list')
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('task-definition') }}"><i class="nav-icon la la-comment"></i> Task definitions</a></li>
@endcan
<hr>
@canany(['user.list', 'role.list', 'permission.list'])
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        @can('user.list')
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        @endcan
        @can('role.list')
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        @endcan
        @can('permission.list')
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
        @endcan
    </ul>
</li>
@endcanany

@canany(['task_definition_type.list', 'plant_family.list', 'saying.list'])
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Application</a>
    <ul class="nav-dropdown-items">
        @can('task_definition_type.list')
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('task-definition-type') }}"><i class="nav-icon la la-list"></i> Task definition types</a></li>
        @endcan
        @can('plant_family.list')
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('plant-family') }}"><i class="nav-icon la la-tree"></i> Plant families</a></li>
        @endcan
        @can('saying.list')
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('saying') }}"><i class="nav-icon la la-comment"></i> Sayings</a></li>
        @endcan
    </ul>
</li>
@endcanany
