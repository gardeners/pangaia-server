<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title inertia>{{ config('app.name') }}</title>

    <!-- HTML Meta Tags -->
    <meta name="description" content="undefined">

    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://pangaia.app">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Pangaia">
    <meta property="og:description" content="A web and mobile application that makes you play and discover the plants nearby and that helps and assists gardeners in planning their tasks">
    <meta property="og:image" content="https://pangaia.app/GraphicRessources/Images/logo-white.svg">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="twitter:domain" content="pangaia.app">
    <meta property="twitter:url" content="https://pangaia.app">
    <meta name="twitter:title" content="Pangaia">
    <meta name="twitter:description" content="A web and mobile application that makes you play and discover the plants nearby and that helps and assists gardeners in planning their tasks">
    <meta name="twitter:image" content="https://pangaia.app/GraphicRessources/Images/logo-white.svg">

    <!-- Meta Tags Generated via https://www.opengraph.xyz -->


    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Matomo -->
    <script>
        var _paq = window._paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
        var u="//matomo.pangaia.app/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
    </script>
    <!-- End Matomo Code -->
  

    <!-- Scripts -->
    @routes
    @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
    @inertiaHead
</head>

<body class="font-sans antialiased overflow-x-hidden">
    @inertia
</body>

</html>